import React from 'react';

import { Box, Button, Picture, Spinner } from 'components/index';
import * as HomeContent from 'utils/pagesContent/home';
import 'assets/style/pages_specific/home.scss';
class PageHome extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			offsetX: '',
			offsetY: '',
			friction: 1 / 180,
			imgsLoaded: false
		};
		this._mouseMove = this._mouseMove.bind(this);
	}

	componentDidMount() {
		document.addEventListener('mousemove', this._mouseMove);

		const $imagesHome = HomeContent.images;

		const loadImage = (image) => {
			return new Promise((resolve, reject) => {
				const loadImg = new Image();
				loadImg.src = image;
				loadImg.onload = () => resolve(image);
				loadImg.onerror = (err) => reject(err);
			});
		};

		Promise.all($imagesHome.map((image) => loadImage(image)))
			.then(() => this.setState({ imgsLoaded: true }))
			.catch((err) => console.log('Failed to load images', err));
	}

	componentWillUnmount() {
		document.removeEventListener('mousemove', this._mouseMove);
	}

	_mouseMove(e) {
		let followX = window.innerWidth / 2 - e.clientX;
		let followY = window.innerHeight / 2 - e.clientY;

		let x = 0,
			y = 0;
		x += (-followX - x) * this.state.friction;
		y += (followY - y) * this.state.friction;

		this.setState({
			offsetX: x,
			offsetY: y
		});
	}

	render() {
		let offset = {
			transform: `translate(-50%, -50%) perspective(600px)
					rotateY(${this.state.offsetX}deg)
					rotateX(${this.state.offsetY}deg)`
		};

		return (
			<React.Fragment>
				{
					this.state.imgsLoaded === true ? <Box className='home'>
						<Box className='home-illuScaleWrapper'>
							<Box className='home-illuAnimWrapper' style={offset}>
								<Box className='home-mainBloc'>
									<Box variant='span' className='home-prezWrapper'>
										<Box variant='span' className='home-nameBloc'>
											<Box variant='span' className='name'>
												<Picture src={HomeContent.Home_Name} notanimated />
											</Box>
											<Box variant='span' className='home-nameBckgrd'>
												<Picture src={HomeContent.Home_Name_Outlines} notanimated />
											</Box>
										</Box>
										<Box variant='span' className='home-portfolio'>
											<Picture src={HomeContent.Home_Portfolio} notanimated />
										</Box>
									</Box>
									<Box className='home-buttonsWrapper'>
										<Button path='/about' label='Voir le profil' />
										<Button path='/project/oodibee' label='Voir les projets' />
									</Box>
								</Box>
								<Box className='home-backgroundVisuals'>
									<Picture
										src={HomeContent.Home_Dev}
										className='home-backgroundVisuals_dev'
										notanimated
									/>
									<Picture
										src={HomeContent.Home_Draw}
										className='home-backgroundVisuals_draw'
										notanimated
									/>
									<Picture
										src={HomeContent.Home_Ondes}
										className='home-backgroundVisuals_ondes'
										notanimated
									/>
									<Picture
										src={HomeContent.Home_Responsive}
										className='home-backgroundVisuals_responsive'
										notanimated
									/>
									<Picture
										src={HomeContent.Home_Border}
										className='home-backgroundVisuals_border'
										notanimated
									/>
								</Box>
							</Box>
						</Box>
					</Box> :
					<Spinner message='Chargement' fullScreen/>}
			</React.Fragment>
		);
	}
}

export default PageHome;
