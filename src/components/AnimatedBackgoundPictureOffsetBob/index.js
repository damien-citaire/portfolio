import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { Box, Picture } from '../index';

import { validatedFormatedProps } from 'utils';

import './style.scss';

const COLORS = [ 'blue', 'red', 'orange', 'purple' ];
const COLOR_DEFAULT = 'blue';

const AnimatedBackgoundPictureOffsetBob = (props) => {
    function colorValidator(bckgrdColor) {
        let color = validatedFormatedProps(bckgrdColor, COLORS, COLOR_DEFAULT);
        return color;
    }

    let animatedBackgoundPictureOffsetBobClasses = classNames(
        props.className,
        'animatedBackgoundPictureOffsetBob',
        { [`animatedBackgoundPictureOffsetBob_color${colorValidator(props.color)}`]: props.color }
    );

    return (
        <Box className={animatedBackgoundPictureOffsetBobClasses}>
            <Box className="animatedBackgoundPictureOffsetBob-pictureWrapper">
                <Picture
                    className="animatedBackgoundPictureOffsetBob-picture"
                    src={props.picture}
                    alt={props.alt}
                />
            </Box>
            <Box variant='span' className="animatedBackgoundPictureOffsetBob-background" />
        </Box>
    );
};

AnimatedBackgoundPictureOffsetBob.defaultProps = {
    alt: '',
    color: COLOR_DEFAULT
};

AnimatedBackgoundPictureOffsetBob.propTypes = {
    className: PropTypes.string,
    picture: PropTypes.string,
    alt: PropTypes.string,
    color: PropTypes.oneOf(COLORS)
};

export default AnimatedBackgoundPictureOffsetBob;
