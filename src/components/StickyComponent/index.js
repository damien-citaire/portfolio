import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { Box } from '../index';

import './style.scss';

const StickyComponent = (props) => {
    let cellClasses = classNames(
        props.className,
        'stickyComponent',
    );

    return (
        <Box className={cellClasses}>
            <Box className="stickyComponent-contentWrapper">
                {props.children}
            </Box>
        </Box>
    );
};

StickyComponent.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node.isRequired
};

export default StickyComponent;