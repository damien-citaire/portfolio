//
// Import Images
// ---
import Opla_Cdv from 'assets/images/projects/Opla_Cdv.png';
import Opla_App_Screens_AddBotDialog from 'assets/images/projects/Opla_App_Screens_AddBotDialog.jpg';
import Opla_App_Screens_Builder from 'assets/images/projects/Opla_App_Screens_Builder.jpg';
import Opla_App_Screens_EditLocalEntity from 'assets/images/projects/Opla_App_Screens_EditLocalEntity.jpg';
import Opla_App_Screens_EntityDetail from 'assets/images/projects/Opla_App_Screens_EntityDetail.jpg';
import Opla_App_Screens_ExtensionsList from 'assets/images/projects/Opla_App_Screens_ExtensionsList.jpg';
import Opla_App_Screens_Flowchart from 'assets/images/projects/Opla_App_Screens_Flowchart.jpg';
import Opla_App_Screens_Flowchart_Edit from 'assets/images/projects/Opla_App_Screens_Flowchart_Edit.jpg';
import Opla_App_Screens_Flowchart_HGOpen from 'assets/images/projects/Opla_App_Screens_Flowchart_HGOpen.jpg';
import Opla_App_Screens_Sign from 'assets/images/projects/Opla_App_Screens_Sign.jpg';
import Opla_App_Screens_UsersList from 'assets/images/projects/Opla_App_Screens_UsersList.jpg';
import Opla_Bobs_Assistant from 'assets/images/projects/Opla_Bobs_Assistant.svg';
import Opla_Bobs_Casdusage from 'assets/images/projects/Opla_Bobs_Casdusage.svg';
import Opla_Bobs_Cloud_Premise from 'assets/images/projects/Opla_Bobs_Cloud_Premise.svg';
import Opla_Bobs_Diversity from 'assets/images/projects/Opla_Bobs_Diversity.svg';
import Opla_Bobs_Hiring from 'assets/images/projects/Opla_Bobs_Hiring.svg';
import Opla_Bobs_NLP from 'assets/images/projects/Opla_Bobs_NLP.svg';
import Opla_Bobs_ProcessusProjet from 'assets/images/projects/Opla_Bobs_ProcessusProjet.svg';
import Opla_Bobs_Technology from 'assets/images/projects/Opla_Bobs_Technology.svg';
import Opla_OplaWorld from 'assets/images/projects/Opla_OplaWorld.jpg';
import Opla_OplaWorld_Autel from 'assets/images/projects/Opla_OplaWorld_Autel.png';
import Opla_OplaWorld_BobDuo from 'assets/images/projects/Opla_OplaWorld_BobDuo.png';
import Opla_OplaWorld_BobLevier from 'assets/images/projects/Opla_OplaWorld_BobLevier.png';
import Opla_OplaWorld_BobScared from 'assets/images/projects/Opla_OplaWorld_BobScared.png';
import Opla_OplaWorld_Computer from 'assets/images/projects/Opla_OplaWorld_Computer.png';
import Opla_OplaWorld_CylinderLava from 'assets/images/projects/Opla_OplaWorld_CylinderLava.png';
import Opla_OplaWorld_FlowchartBlocs from 'assets/images/projects/Opla_OplaWorld_FlowchartBlocs.png';
import Opla_OplaWorld_GlassesBlocs from 'assets/images/projects/Opla_OplaWorld_GlassesBlocs.png';
import Opla_Website from 'assets/images/projects/Opla_Website.jpg';
//

//
// Stock Images in const for Loading Sections
// ---
export const sectionPrez = [
	Opla_Cdv
];

export const sectionBobs = [
	Opla_Bobs_Assistant,
    Opla_Bobs_Casdusage,
    Opla_Bobs_Technology,
    Opla_Bobs_ProcessusProjet,
    Opla_Bobs_Diversity,
    Opla_Bobs_NLP,
    Opla_Bobs_Cloud_Premise,
    Opla_Bobs_Hiring
];

export const sectionWebsite = [
	Opla_Website
];

export const sectionOplaWorld = [
	Opla_OplaWorld_BobDuo,
	Opla_OplaWorld_BobScared,
	Opla_OplaWorld_BobLevier,
    Opla_OplaWorld_Autel,
    Opla_OplaWorld_GlassesBlocs,
    Opla_OplaWorld_CylinderLava,
    Opla_OplaWorld_FlowchartBlocs,
    Opla_OplaWorld_Computer,
    Opla_OplaWorld
];

export const sectionAppScreen = [
	Opla_App_Screens_EntityDetail,
	Opla_App_Screens_EditLocalEntity,
	Opla_App_Screens_Sign,
    Opla_App_Screens_ExtensionsList,
    Opla_App_Screens_Builder,
    Opla_App_Screens_UsersList,
    Opla_App_Screens_AddBotDialog,
    Opla_App_Screens_Flowchart,
    Opla_App_Screens_Flowchart_HGOpen,
    Opla_App_Screens_Flowchart_Edit
];
//

//
// Export Images
// ---
export {
    Opla_Cdv,
    Opla_App_Screens_AddBotDialog,
    Opla_App_Screens_Builder,
    Opla_App_Screens_EditLocalEntity,
    Opla_App_Screens_EntityDetail,
    Opla_App_Screens_ExtensionsList,
    Opla_App_Screens_Flowchart,
    Opla_App_Screens_Flowchart_Edit,
    Opla_App_Screens_Flowchart_HGOpen,
    Opla_App_Screens_Sign,
    Opla_App_Screens_UsersList,
    Opla_Bobs_Assistant,
    Opla_Bobs_Casdusage,
    Opla_Bobs_Cloud_Premise,
    Opla_Bobs_Diversity,
    Opla_Bobs_Hiring,
    Opla_Bobs_NLP,
    Opla_Bobs_ProcessusProjet,
    Opla_Bobs_Technology,
    Opla_OplaWorld,
    Opla_OplaWorld_Autel,
    Opla_OplaWorld_BobDuo,
    Opla_OplaWorld_BobLevier,
    Opla_OplaWorld_BobScared,
    Opla_OplaWorld_Computer,
    Opla_OplaWorld_CylinderLava,
    Opla_OplaWorld_FlowchartBlocs,
    Opla_OplaWorld_GlassesBlocs,
    Opla_Website
}
//