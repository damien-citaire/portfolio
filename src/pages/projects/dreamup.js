import React from 'react';
import LazyLoad from 'react-lazyload';

import { ProjectPresentation } from 'layouts/index';
import {
	AnimatedBackgoundPicture,
	Box,
	Cell,
	Container,
	FrameBorder,
	Loading,
	Picture,
	PictureEnvelope,
	ProjectDescription,
	StickyComponent,
	Typography,
	Video
} from 'components/index';
import Dreamup_Isometric_Schema from 'assets/videos/Dreamup_Isometric_Schema.mp4';
import * as DreamupContent from 'utils/pagesContent/dreamup';
import 'assets/style/pages_specific/dreamup.scss';

const PageDreamup = () => {
	function DreamUpDescription(){
		return (
			<React.Fragment>
				<Typography variant='body1' color='secondary'>
					Solution en ligne axée sur la personnalisation de lunettes s’adressant à
					l’ensemble des acteurs jouant un rôle dans la conception, la fabrication et la
					vente de montures sur-mesure.
				</Typography>
				<Typography variant='body1' color='secondary'>
					Composée d’une application par pôle d’activité, la plateforme facilite les
					échanges entre Designer, Fabricant et Opticien afin d’offrir au Client final la
					possibilité d’accéder à un catalogue de montures personnalisables et adaptables
					à leur biométrie.
				</Typography>
				<Typography variant='body1' color='secondary'>
					Via un système d’essayage assisté par ordinateur réaliste, le client peut
					prévisualiser les modèles adaptés sur son visage et ainsi co-créer avec son
					opticien une monture unique dont les adaptations morphologiques respectent les
					contraintes métiers et garantissent un fichier de fabrication sans risque
					d’erreurs.
				</Typography>
				<br />
				<Typography variant='body1' color='secondary'>
					<b>Domaines abordés :</b> Identité visuelle, Webdesign, Design d’interface,
					Communication Web & Print, Motion design...
				</Typography>
				<br />
				<Typography variant='body1' color='secondary'>
					Période de réalisation entre <b>2016</b> et <b>2018</b>
				</Typography>
				<Typography variant='body1' color='secondary'>
					Projet mené au sein de l’entreprise <b>Polymagine</b>
				</Typography>
			</React.Fragment>
		);
	}

	return (
		<ProjectPresentation
			projectTitle='DreamUp 3D - Eyewear'
			projectNumber={2}
			prevProject='oodibee'
			nextProject='svialle'
			sectionPrez={DreamupContent.sectionPrez}
			projectDescription={DreamUpDescription()}
			projectSamplePicture={DreamupContent.DreamUp_Charte}
			projectSampleTitle='Identité visuelle'
			projectSampleDescription='Création du logo DreamUp 3D et définition de la charte graphique'
		>
			<Container>
				<Cell row>
					<FrameBorder animated>
						<Loading lazyLoadHeight={930} images={DreamupContent.sectionVisuelsPrez}>
							<Cell row>
								<Cell lg={5} md={8} className='u-marginTopAuto'>
									<ProjectDescription title='Visuels d’illustration' right bottom>
										Recherches et création de visuels de présentation autour du
										concept proposé par la marque Dreamup 3D - Eyewear
									</ProjectDescription>
									<Picture src={DreamupContent.DreamUp_PrezVisu_Concept} />
								</Cell>
								<Cell lg={2} md={4} disablePadding className='u-alignItemsCenter'>
									<Cell md={12} sm={6} xs={6}>
										<Picture
											src={DreamupContent.DreamUp_PrezVisu_Blueprint_White}
										/>
									</Cell>
									<Cell md={12} sm={6} xs={6}>
										<Picture
											src={DreamupContent.DreamUp_PrezVisu_Blueprint_Blue}
										/>
									</Cell>
								</Cell>
								<Cell lg={5} md={6} className='u-alignSelfCenter u-padding5'>
									<Picture src={DreamupContent.DreamUp_PrezVisu_2D_3D} />
								</Cell>
								<Cell disablePadding lg={12} md={6}>
									<Cell lg={6}>
										<Picture
											src={DreamupContent.DreamUp_PrezVisu_Arrivee_Branches}
										/>
									</Cell>
									<Cell lg={6}>
										<Picture src={DreamupContent.DreamUp_PrezVisu_Kit} />
									</Cell>
								</Cell>
							</Cell>
						</Loading>

						<Loading lazyLoadHeight={1111} images={DreamupContent.sectionSiteMap}>
							<Cell row>
								<Cell>
									<AnimatedBackgoundPicture
										className='u-paddingTop0'
										picture={DreamupContent.DreamUp_Sitemap_FullInSituation}
									/>
								</Cell>
								<Cell md={4} sm={6} className='u-alignItemsFlexStart'>
									<Box className='background_colorGrayLight'>
										<Picture src={DreamupContent.DreamUp_Sitemap_PreviewPart} />
									</Box>
								</Cell>
								<Cell md={8} sm={6} disablePadding>
									<Cell md={6}>
										<Box className='background_colorGrayLight'>
											<Picture src={DreamupContent.DreamUp_Sitemap_Screens} />
										</Box>
									</Cell>
									<Cell md={6}>
										<Box className='background_colorGrayLight'>
											<Picture
												src={DreamupContent.DreamUp_Sitemap_Preview_Screen}
											/>
										</Box>
									</Cell>
									<Cell md={10}>
										<ProjectDescription title='Flowchart'>
											Réflexion et représentation sous forme de flowchart de
											l’ensemble de la navigation proposée pour chacune des 3
											applications de la plate-forme
										</ProjectDescription>
									</Cell>
								</Cell>
							</Cell>
						</Loading>
					</FrameBorder>
				</Cell>
			</Container>
			<Loading lazyLoadHeight={2016} images={DreamupContent.sectionBluePrintComponents}>
				<Container fluid className='background_bluePrintDreamUp background_fadeApparition'>
					<Container>
						<Cell row>
							<Cell className='background_bluePrintDreamUpOverlay'>
								<Cell
									lg={4}
									md={12}
									disablePadding
									dirColumn
									className='u-justifyFlexEnd u-alignItemsFlexEnd u-marginTopAuto'
								>
									<ProjectDescription
										title='Design d’interface et intégration'
										right
										className='u-marginBottomAuto'
									>
										Création d’une bibliothèque de composants graphiques
										sur-mesure permettant la construction de la plate-forme
									</ProjectDescription>
									<Picture
										src={DreamupContent.DreamUp_Blueprint_Components_pt1}
										className='u-marginTop4'
									/>
								</Cell>
								<Cell
									lg={8}
									md={12}
									className='u-alignItemsFlexEnd u-justifyCenter'
								>
									<Picture
										src={DreamupContent.DreamUp_Blueprint_Components_pt2}
									/>
								</Cell>
							</Cell>
						</Cell>
					</Container>
				</Container>
			</Loading>

			<Loading lazyLoadHeight={742} images={DreamupContent.sectionAnim404}>
				<Container className='u-marginY4'>
					<FrameBorder animated>
						<Cell row className='u-alignItemsStretch'>
							<Cell md={5} className='u-alignItemsCenter u-justifyCenter'>
								<Picture src={DreamupContent.DreamUp_Animated_Logo} />
							</Cell>
							<Cell md={1} />
							<Cell md={5} className='u-alignItemsCenter u-justifyCenter'>
								<Picture src={DreamupContent.DreamUp_App404} />
							</Cell>
							<Cell md={1} />
							<Cell md={5} className='u-alignItemsCenter u-justifyCenter'>
								<Picture src={DreamupContent.DreamUp_AppSpinner3d} />
							</Cell>
							<Cell md={7}>
								<ProjectDescription
									title='Visuels d’illustration | Motion Design'
									bottom
									right
								>
									Créations de visuel pour la page 404 et d'animations pour les
									écrans de chargement/processing
								</ProjectDescription>
							</Cell>
						</Cell>
					</FrameBorder>
				</Container>
			</Loading>

			<Loading lazyLoadHeight={2105} images={DreamupContent.sectionAppScreen}>
				<Container
					fluid
					className='background_radialGradientDreamUp background_fadeApparition'
				>
					<Container>
						<Cell row>
							<Cell lg={4} md={6} className='u-marginTopAuto' disablePadding>
								<Cell disablePadding>
									<Cell>
										<ProjectDescription
											title='Design d’interface et intégration'
											top
											right
										>
											Création et intégration de l’interface de la
											plate-forme. Aperçus de différents écrans des
											applications Designer et Opticien
										</ProjectDescription>
									</Cell>
									<Cell>
										<Picture
											className='u-shadow6'
											src={DreamupContent.DreamUp_AppScreens_ListingModels}
										/>
									</Cell>
									<Cell>
										<Picture
											className='u-shadow6'
											src={DreamupContent.DreamUp_AppScreens_Config_Trace}
										/>
									</Cell>
								</Cell>
							</Cell>
							<Cell lg={8} md={6} disablePadding>
								<Cell>
									<Picture
										className='u-shadow6'
										src={DreamupContent.DreamUp_AppScreens_CatalogOpti_Face}
									/>
								</Cell>
								<Cell lg={6} md={12}>
									<Picture
										className='u-shadow6'
										src={DreamupContent.DreamUp_AppScreens_Branch}
									/>
								</Cell>
								<Cell lg={6} md={12}>
									<Picture
										className='u-shadow6'
										src={DreamupContent.DreamUp_AppScreens_3DModel_Export}
									/>
								</Cell>
							</Cell>
							<Cell lg={7} className='u-alignItemsFlexStart' disablePadding>
								<Cell lg={12} md={6}>
									<Picture
										className='u-shadow6'
										src={DreamupContent.DreamUp_AppScreens_Config}
									/>
								</Cell>
								<Cell lg={12} md={6}>
									<Picture
										className='u-shadow6'
										src={DreamupContent.DreamUp_AppScreens_Catalog}
									/>
								</Cell>
							</Cell>
							<Cell lg={5} className='u-alignItemsFlexStart' disablePadding>
								<Cell disablePadding>
									<Cell lg={12} md={6}>
										<Picture
											className='u-shadow6'
											src={
												DreamupContent.DreamUp_AppScreens_CatalogOpt_Charnieres
											}
										/>
									</Cell>
									<Cell lg={12} md={6}>
										<Picture
											className='u-shadow6'
											src={DreamupContent.DreamUp_AppScreens_RecapCommand}
										/>
									</Cell>
									<Cell lg={12} md={6}>
										<Picture
											className='u-shadow6'
											src={DreamupContent.DreamUp_AppScreens_SearchClient}
										/>
									</Cell>
								</Cell>
							</Cell>
						</Cell>
					</Container>
				</Container>
			</Loading>
			<Container>
				<Cell row>
					<Loading lazyLoadHeight={828} images={DreamupContent.sectionBonCommande}>
						<Cell row className='u-justifyCenter'>
							<Cell lg={4} md={6} className='u-justifyCenter'>
								<Picture src={DreamupContent.DreamUp_BonsComande_Formes} />
							</Cell>
							<Cell lg={4} md={6} className='u-justifyCenter'>
								<Picture src={DreamupContent.DreamUp_BonsComande_SurMesures} />
							</Cell>
							<Cell lg={4} md={6} className='u-justifyCenter'>
								<Picture src={DreamupContent.DreamUp_BonsComande_Outillage} />
							</Cell>
							<Cell>
								<ProjectDescription title='Communication Web' right>
									Création de documents PDF interactifs afin d’accompagner les
									opticiens dans leur premier achat sur la plate-forme
								</ProjectDescription>
							</Cell>
						</Cell>
					</Loading>
				</Cell>
			</Container>
			<Loading lazyLoadHeight={802} images={DreamupContent.sectionInvits}>
				<Container fluid className='background_colorGrayLight background_fadeApparition'>
					<Container>
						<Cell row>
							<Cell className='u-marginBottom2'>
								<Cell lg={5} md={6}>
									<Picture
										className='u-shadow4'
										src={DreamupContent.DreamUp_Print_Invit_LunetteParty_Recto}
									/>
								</Cell>
								<Cell lg={5} md={6}>
									<PictureEnvelope
										right
										picture={
											DreamupContent.DreamUp_Print_Invit_LunetteParty_Verso
										}
									/>
								</Cell>
								<Cell lg={2} />
							</Cell>
							<Cell>
								<Cell lg={2} />
								<Cell lg={5} md={6}>
									<PictureEnvelope
										picture={DreamupContent.DreamUp_Print_Invit_Silmo_Recto}
									/>
								</Cell>
								<Cell lg={5} md={6}>
									<Picture
										className='u-shadow4'
										src={DreamupContent.DreamUp_Print_Invit_Silmo_Verso}
									/>
								</Cell>
								<Cell>
									<ProjectDescription title='Print' right>
										Création de cartons d’invitations aux différents salons
										auxquel la marque sera présente
									</ProjectDescription>
								</Cell>
							</Cell>
						</Cell>
					</Container>
				</Container>
			</Loading>
			<Container>
				<Cell row>
					<FrameBorder animated>
						<Loading lazyLoadHeight={1056} images={DreamupContent.sectionPrint}>
							<Cell row>
								<Cell className='u-background_radialGradientDreamUp'>
									<Picture src={DreamupContent.DreamUp_Print_Brochure_A4} />
								</Cell>
								<Cell lg={4}>
									<ProjectDescription title='Print' right top>
										Création de plaquettes de présentation et de cartes de
										visites visant à être distribués pendant les salons auxquels
										la marque sera présente
									</ProjectDescription>
								</Cell>
								<Cell lg={8}>
									<AnimatedBackgoundPicture
										picture={DreamupContent.DreamUp_Print_Cdv}
									/>
								</Cell>
							</Cell>
						</Loading>
					</FrameBorder>
				</Cell>
			</Container>
			<Loading lazyLoadHeight={955} images={DreamupContent.sectionPlv}>
				<Container className='background_wallDreamUp background_fadeApparition' fluid>
					<Container>
						<Cell row disablePadding>
							<Cell lg={1} md={12} sm={2} disablePaddingY />
							<Cell
								disablePaddingY
								lg={5}
								md={6}
								sm={8}
								className='u-paddingBottom9 u-alignItemsFlexStart'
							>
								<Picture src={DreamupContent.DreamUp_Print_Affiche_Salon} />
							</Cell>
							<Cell lg={1} md={1} sm={2} disablePaddingY />
							<Cell
								disablePaddingY
								lg={4}
								md={5}
								sm={9}
								className='u-paddingTop10 u-alignItemsFlexEnd'
							>
								<Picture src={DreamupContent.DreamUp_Print_RollUp} />
							</Cell>
							<Cell lg={1} md={12} sm={2} disablePaddingY />
							<Cell>
								<ProjectDescription title='PLV'>
									Création de visuels destinés aux affiches et roll’ups promouvant
									la plate-forme dans les salons
								</ProjectDescription>
							</Cell>
						</Cell>
					</Container>
				</Container>
			</Loading>
			<Loading lazyLoadHeight={2406} images={DreamupContent.sectionVideoComponents}>
				<Box className='videoComponents-wrapper u-marginBottom5 background_fadeApparition'>
					<Container>
						<FrameBorder animated>
							<Container className='background_radialGradientDreamUp'>
								<Cell row>
									<Cell lg={4} md={4}>
										<Cell sm={1} disablePadding />
										<Cell sm={10}>
											<Cell md={12} sm={3} disablePadding />
											<Cell md={12} sm={6}>
												<Picture
													src={
														DreamupContent.DreamUp_Video_Components_Tool_Cephalo
													}
												/>
											</Cell>
											<Cell md={12} sm={3} disablePadding />
											<Cell
												md={12}
												sm={6}
												className='videoComponents-toolRhino'
											>
												<Picture
													src={
														DreamupContent.DreamUp_Video_Components_Tool_Rhinometre
													}
												/>
											</Cell>
											<Cell
												md={12}
												sm={6}
												className='videoComponents-toolReglet'
											>
												<Picture
													src={
														DreamupContent.DreamUp_Video_Components_Tool_Reglet
													}
												/>
											</Cell>
										</Cell>
										<Cell sm={1} disablePadding />
									</Cell>
									<Cell lg={8} md={8}>
										<Cell className='videoComponents-3dGlasses'>
											<Picture
												src={
													DreamupContent.DreamUp_Video_Components_3D_Glasses
												}
											/>
										</Cell>
										<Cell
											disablePadding
											className='videoComponents-tools u-alignItemsFlexEnd u-justifyCenter'
										>
											<Cell md={3} xs={7}>
												<Picture
													src={
														DreamupContent.DreamUp_Video_Components_iPad
													}
												/>
											</Cell>
											<Cell
												md={6}
												xs={5}
												className='u-alignItemsFlexEnd u-justifySpaceAround'
											>
												<Cell md={5}>
													<Picture
														src={
															DreamupContent.DreamUp_Video_Components_App_Photo
														}
													/>
												</Cell>
												<Cell md={5}>
													<Picture
														src={
															DreamupContent.DreamUp_Video_Components_Tool_Pupilo
														}
													/>
												</Cell>
											</Cell>
										</Cell>
									</Cell>
								</Cell>
							</Container>
							<Container className='background_radialGradientDreamUp videoComponents-ricksAndMorties'>
								<Cell row>
									<Cell lg={6} disablePadding>
										<Cell>
											<ProjectDescription
												title='Illustration vectorielle'
												className='videoComponents-description'
												center
											>
												Création d’illustrations vectorielles destinées à
												être animées au sein d’une vidéo de
												promotion-présentation de la plate-forme
											</ProjectDescription>
										</Cell>
										<Cell xs={6}>
											<Picture
												src={
													DreamupContent.DreamUp_Video_Components_Morty_Buste
												}
											/>
										</Cell>
										<Cell xs={6}>
											<Picture
												src={
													DreamupContent.DreamUp_Video_Components_Morty_Buste_Glasses
												}
											/>
										</Cell>
										<Cell md={1} disablePadding />
										<Cell md={10} className='u-marginTop3'>
											<Picture
												src={
													DreamupContent.DreamUp_Video_Components_Morty_Faces
												}
											/>
										</Cell>
										<Cell md={1} disablePadding />
									</Cell>
									<Cell md={1} disablePadding />
									<Cell lg={4} md={10} className='u-alignItemsCenter'>
										<Picture
											src={DreamupContent.DreamUp_Video_Components_Rick_Morty}
										/>
									</Cell>
									<Cell md={1} disablePadding />
								</Cell>
							</Container>
							<Cell>
								<ProjectDescription title='Illustration vectorielle' right>
									Création d’illustrations schématisant différents écrans pour la
									vidéo
								</ProjectDescription>
							</Cell>
							<Container className='background_radialGradientDreamUp'>
								<Cell row className='u-justifyCenter'>
									<Cell lg={4} sm={6}>
										<Picture
											className='u-shadow6'
											src={
												DreamupContent.DreamUp_Video_Components_Screen_4views
											}
										/>
									</Cell>
									<Cell lg={4} sm={6}>
										<Picture
											className='u-shadow6'
											src={
												DreamupContent.DreamUp_Video_Components_Screen_Adaptation
											}
										/>
									</Cell>
									<Cell lg={4} sm={6}>
										<Picture
											className='u-shadow6'
											src={
												DreamupContent.DreamUp_Video_Components_Screen_Catalog
											}
										/>
									</Cell>
								</Cell>
							</Container>
						</FrameBorder>
					</Container>
				</Box>
			</Loading>
			<LazyLoad height={1070} offset={-200}>
				<Container fluid className='background_video background_fadeApparition'>
					<Container className='u-paddingBottom5'>
						<Cell row className='u-justifyCenter'>
							<Cell>
								<ProjectDescription
									title='Motion design'
									className='u-marginBottom5'
									top
								>
									Réalisation d’une vidéo en animation visant à promouvoir et
									présenter la plate-forme DreamUp 3D - Eyewear
								</ProjectDescription>
							</Cell>
							<Cell md={10}>
								<Video src={Dreamup_Isometric_Schema} controls />
							</Cell>
						</Cell>
					</Container>
				</Container>
			</LazyLoad>

			<Loading lazyLoadHeight={3584} images={DreamupContent.sectionWebsite}>
				<Container fluid className='background_colorGrayLighter background_fadeApparition'>
					<Container>
						<Cell row className='u-alignItemsStretch'>
							<Cell lg={4} md={6} sm={12} className='u-alignItemsCenter'>
								<StickyComponent>
									<Picture
										className='u-shadow6 u-marginTop2 u-marginBottom1'
										src={DreamupContent.DreamUp_WebSite_Designer}
										notanimated
									/>
									<Picture
										className='u-shadow6 u-marginY1'
										src={DreamupContent.DreamUp_WebSite_Formulaire}
										notanimated
									/>
									<ProjectDescription
										title='Webdesign - Intégration'
										center
										right
										className='u-marginTop2'
									>
										Création et intégration des mockups du site vitrine DreamUp
										3D - Eyewear
									</ProjectDescription>
								</StickyComponent>
							</Cell>
							<Cell lg={8} md={6} sm={12} className='u-justifyCenter'>
								<Picture
									className='u-shadow6 u-marginY2'
									src={DreamupContent.DreamUp_WebSite_Index}
									notanimated
								/>
								<Picture
									className='u-shadow6 u-marginY2'
									src={DreamupContent.DreamUp_WebSite_Opticien}
									notanimated
								/>
							</Cell>
						</Cell>
					</Container>
				</Container>

				<Loading lazyLoadHeight={519} images={DreamupContent.sectionSocial}>
					<Container>
						<Cell row>
							<Cell>
								<ProjectDescription title='Communication Web'>
									Création de visuels de profil et de couverture pour les réseaux
									sociaux
								</ProjectDescription>
							</Cell>
							<Cell lg={12}>
								<Cell md={6} disablePadding>
									<AnimatedBackgoundPicture
										className='u-padding4'
										color='dreamup'
										picture={DreamupContent.DreamUp_Social_Facebook}
									/>
								</Cell>
								<Cell md={6} disablePadding>
									<AnimatedBackgoundPicture
										className='u-padding4'
										color='dreamup'
										picture={DreamupContent.DreamUp_Social_Twitter}
									/>
								</Cell>
							</Cell>
						</Cell>
					</Container>
				</Loading>
			</Loading>
		</ProjectPresentation>
	);
};

export default PageDreamup;
