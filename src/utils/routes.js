//
// Export all routes
// -----------------------------

import React from 'react';

const PageHome = React.lazy(() => import('pages/home'));
const PageAbout = React.lazy(() => import('pages/about'));
const PageProjectOodibee = React.lazy(() => import('pages/projects/oodibee'));
const PageProjectDreamup = React.lazy(() => import('pages/projects/dreamup'));
const PageProjectVialle = React.lazy(() => import('pages/projects/vialle'));
const PageProjectPolymagine = React.lazy(() => import('pages/projects/polymagine'));
const PageProjectOpla = React.lazy(() => import('pages/projects/opla'));

export const routes = [
	{ path: '/', name: 'Home', Component: PageHome },
	{ path: '/about', name: 'About', Component: PageAbout },
	{ path: '/project/oodibee', name: 'Oodibee', Component: PageProjectOodibee },
	{ path: '/project/dreamup', name: 'Dreamup 3D Eyewear', Component: PageProjectDreamup },
	{ path: '/project/svialle', name: 'SVialle', Component: PageProjectVialle },
	{ path: '/project/polymagine', name: 'Polymagine', Component: PageProjectPolymagine },
	{ path: '/project/opla', name: 'Opla', Component: PageProjectOpla }
];


export const projectsRoutes = [
	{ path: '/project/oodibee', name: 'Oodibee' },
	{ path: '/project/dreamup', name: 'Dreamup 3D Eyewear' },
	{ path: '/project/svialle', name: 'SVialle' },
	{ path: '/project/polymagine', name: 'Polymagine' },
	{ path: '/project/opla', name: 'Opla' }
];