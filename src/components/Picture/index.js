import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { CSSTransition } from 'react-transition-group';

import './style.scss';

class Picture extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			mounted: false
		};
	}
	componentDidMount() {
		this.setState({
			mounted: true
		});
	}

	render() {
		let pictureClasses = classNames(this.props.className, 'picture');

		return (
			<React.Fragment>
				{
					this.props.notanimated ? <img
						{...this.props}
						className={pictureClasses}
						src={this.props.src}
						alt={this.props.alt}
					/> :
					<CSSTransition
						classNames={{
							appear: 'picture_animatedAppear',
							appearActive: 'picture_animatedAppearActive',
							appearDone: 'picture_animatedAppearDone',
							enter: 'picture_animatedEnter',
							enterActive: 'picture_animatedEnterActive',
							enterDone: 'picture_animatedEnterDone',
							exit: 'picture_animatedExit',
							exitActive: 'picture_animatedExitActive',
							exitDone: 'picture_animatedExitDone'
						}}
						timeout={10000}
						in={this.state.mounted}
					>
						<img
							{...this.props}
							className={pictureClasses}
							src={this.props.src}
							alt={this.props.alt}
						/>
					</CSSTransition>}
			</React.Fragment>
		);
	}
}

Picture.defaultProps = {
	alt: '',
	notanimated: false
};

Picture.propTypes = {
	className: PropTypes.string,
	src: PropTypes.string.isRequired,
	alt: PropTypes.string,
	notanimated: PropTypes.bool
};

export default Picture;
