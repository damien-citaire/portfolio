import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { NavLink } from 'react-router-dom';

import { Box, Typography } from '../index';

import './style.scss';

const ProjectsNavigation = (props) => {
	let ProjectsNavigationClasses = classNames('projectsNavigation', {
		projectsNavigation_bottom: props.bottom
	});

	return (
		<Box className={ProjectsNavigationClasses}>
			{props.prevProject && (
				<NavLink
					exact
					key='prev'
					to={'/project/' + props.prevProject}
					className='projectsNavigation-navigationItem projectsNavigation-navigationItem_prev'
				>
					<Box className='projectsNavigation-navigationItemIcon' />
					<Typography
						className='projectsNavigation-navigationItemLabel'
						variant='overline'
					>
						Projet Précédent
					</Typography>
				</NavLink>
			)}
			<Typography
				className='projectsNavigation-navigationStatus'
				variant='overline'
				color='secondary'
			>
				Projet n°{' '}
				<Box variant='span' className='projectsNavigation-navigationStatus_bigger'>
					{props.projectNumber}
				</Box>{' '}
				/ 5
			</Typography>
			{props.nextProject && (
				<NavLink
					exact
					key='next'
					to={'/project/' + props.nextProject}
					className='projectsNavigation-navigationItem projectsNavigation-navigationItem_next'
				>
					<Box className='projectsNavigation-navigationItemIcon' />
					<Typography
						className='projectsNavigation-navigationItemLabel'
						variant='overline'
					>
						Projet Suivant
					</Typography>
				</NavLink>
			)}
		</Box>
	);
};

ProjectsNavigation.propTypes = {
	className: PropTypes.string,
	projectNumber: PropTypes.number,
	prevProject: PropTypes.string,
	nextProject: PropTypes.string
};

export default ProjectsNavigation;
