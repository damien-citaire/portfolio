import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { Box, Picture } from '../index';

import "./style.scss";


const PicturesRevealStrip = (props) => {

    let picturesRevealStripClasses = classNames(
        props.className,
        'picturesRevealStrip',
    );

    return (
        <Box className={picturesRevealStripClasses}>
            <Box className="picturesRevealStrip-pictures">
                <Box className='picturesRevealStrip-picture picturesRevealStrip-picture1'>
                    <Picture src={props.picture1} notanimated />
                </Box>
                <Box className='picturesRevealStrip-picture picturesRevealStrip-picture2'>
                    <Picture src={props.picture2} notanimated />
                </Box>
                <Box className='picturesRevealStrip-picture picturesRevealStrip-picture3'>
                    <Picture src={props.picture3} notanimated />
                </Box>
            </Box>
            <Box className="picturesRevealStrip-strip" />
        </Box>
    );
};

PicturesRevealStrip.propTypes = {
    className: PropTypes.string,
    picture: PropTypes.string,
    alt: PropTypes.string,
};

export default PicturesRevealStrip;