import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { ProjectBigHeader, ProjectsNavigation, Spinner } from 'components/index';

const ProjectPresentation = (props) => {
	const [
		imgsLoaded,
		setImgsLoaded
	] = useState(false);

	useEffect(() => {
		const $imagesPrez = props.sectionPrez;

		const loadImage = (image) => {
			return new Promise((resolve, reject) => {
				const loadImg = new Image();
				loadImg.src = image;
				loadImg.onload = () => resolve(image);
				loadImg.onerror = (err) => reject(err);
			});
		};

		Promise.all($imagesPrez.map((image) => loadImage(image)))
			.then(() => setImgsLoaded(true))
			.catch((err) => console.log('Failed to load images', err));
	});

	return (
		<React.Fragment>
			{
				imgsLoaded ? <React.Fragment>
					<ProjectBigHeader {...props} />
					{props.children}
					<ProjectsNavigation
						bottom
						prevProject={props.prevProject}
						nextProject={props.nextProject}
						projectNumber={props.projectNumber}
					/>
				</React.Fragment> :
				<Spinner fullScreen message='Chargement du Projet' />}
		</React.Fragment>
	);
};

ProjectPresentation.propTypes = {
	sectionPrez: PropTypes.array.isRequired,
	prevProject: PropTypes.string,
	nextProject: PropTypes.string,
	projectNumber: PropTypes.number,
	children: PropTypes.node.isRequired
};

export default ProjectPresentation;
