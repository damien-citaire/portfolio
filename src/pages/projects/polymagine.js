import React from 'react';

import { ProjectPresentation } from 'layouts/index';
import {
	AnimatedBackgoundPicture,
	Box,
	Cell,
	Container,
	FrameBorder,
	Loading,
	Picture,
	PicturesRevealStrip,
	PicturesRevealStrips,
	ProjectDescription,
	StickyComponent,
	Typography
} from 'components/index';
import * as PolymagineContent from 'utils/pagesContent/polymagine';

export default function PagePolymagine(){
	const prezLissacOverlay = {
		position: 'absolute',
		top: 0,
		left: 0,
		height: '100%',
		width: '100%'
	};

	function PolymagineDescription(){
		return (
			<React.Fragment>
				<Typography variant='body1' color='secondary'>
					Société derrière les projets Oodibee et DreamUp 3D, Polymagine propose des
					solutions logicielles pour la personnalisation de la production.
				</Typography>
				<Typography variant='body1' color='secondary'>
					Derrière cette façade se cache une réelle volonté d’accompagner les entreprises
					dans la mise en place d’un processus de co-conception et de fabrication fiable,
					valorisant pour le client final et économiquement rentable.
				</Typography>
				<Typography variant='body1' color='secondary'>
					Visualisation 3D réaliste, interfaces de manipulation et de paramétrage d’objets
					2D & 3D contraints ne sont que quelques-uns des nombreux concepts mis en avant
					par la société.
				</Typography>
				<Typography variant='body1' color='secondary'>
					C’est donc au travers de schémas synoptiques poussés et de présentations
					soignées qu’il a fallu présenter ces innovations.
				</Typography>
				<br />
				<Typography variant='body1' color='secondary'>
					<b>Domaines abordés :</b> Identité visuelle, Visuels d’illustration, Schémas
					synoptiques, Présentations keynotes, Print, Webdesign.
				</Typography>
				<br />
				<Typography variant='body1' color='secondary'>
					Période de réalisation entre <b>2014</b> et <b>2018</b>
				</Typography>
				<Typography variant='body1' color='secondary'>
					Projet mené au sein de l’entreprise <b>Polymagine</b>
				</Typography>
			</React.Fragment>
		);
	}

	return (
		<ProjectPresentation
			projectTitle='Polymagine'
			projectNumber={4}
			prevProject='svialle'
			nextProject='opla'
			sectionPrez={PolymagineContent.sectionPrez}
			projectDescription={PolymagineDescription()}
			projectSamplePicture={PolymagineContent.Polymagine_Charte}
			projectSampleTitle='Identité visuelle'
			projectSampleDescription='Déclinaison du logo et création de la charte graphique Polymagine'
		>
			<Container>
				<Cell row>
					<FrameBorder animated>
						<Loading lazyLoadHeight={618} images={PolymagineContent.sectionPrint}>
							<Cell row>
								<Cell md={8}>
									<AnimatedBackgoundPicture picture={PolymagineContent.Polymagine_CdV} />
								</Cell>
								<Cell md={4}>
									<ProjectDescription title='Print' bottom>
										Création de cartes de visites
									</ProjectDescription>
								</Cell>
							</Cell>
						</Loading>

						<Loading lazyLoadHeight={940} images={PolymagineContent.sectionSchemas}>
							<Cell row>
								<Cell lg={4} md={12}>
									<Cell disablePadding>
										<ProjectDescription title='Schéma synoptique' top>
											Création de schémas synoptiques présentant le concept
											promu par Polymagine, notamment pour sa plate-forme
											DreamUp 3D
										</ProjectDescription>
									</Cell>
									<Cell
										lg={12}
										md={6}
										className='u-alignItemsCenter u-justifyCenter'
									>
										<Picture
											className='u-marginTop5'
											src={PolymagineContent.Polymagine_Schema_PlateForme2}
										/>
									</Cell>
									<Cell
										lg={12}
										md={6}
										className='u-alignItemsCenter u-justifyCenter'
									>
										<Picture
											className='u-marginTop5'
											src={PolymagineContent.Polymagine_Schema_PlateForme3}
										/>
									</Cell>
								</Cell>
								<Cell lg={8} md={12} className='u-alignItemsCenter u-justifyCenter'>
									<Picture src={PolymagineContent.Polymagine_Schema_PlateForme1} />
								</Cell>
							</Cell>
						</Loading>

						<Loading lazyLoadHeight={973} images={PolymagineContent.sectionSchemasAdvanced}>
							<Cell row>
								<Cell lg={8} md={12} className='u-alignItemsCenter u-justifyCenter'>
									<Picture src={PolymagineContent.Polymagine_Schema_Dreamup} />
								</Cell>
								<Cell lg={4} md={12}>
									<Cell disablePadding>
										<ProjectDescription
											title='Schéma synoptique - Illustration vectorielle'
											top
											right
										>
											Variation et étoffement du schéma synoptique avec des
											illustrations vectorielles plus abouties en vue d’une
											présentation spécifique à la plate-forme DreamUp 3D
											Eyewear
										</ProjectDescription>
									</Cell>
									<Cell>
										<PicturesRevealStrip
											className='u-marginXAuto'
											picture1={PolymagineContent.Polymagine_Schema_Illu1}
											picture2={PolymagineContent.Polymagine_Schema_Illu2}
											picture3={PolymagineContent.Polymagine_Schema_Illu3}
										/>
									</Cell>
								</Cell>
							</Cell>
						</Loading>
					</FrameBorder>
				</Cell>
			</Container>

			<Loading lazyLoadHeight={1708} images={PolymagineContent.sectionPrezLissac}>
				<Container fluid className='background_polymaginePanels background_fadeApparition'>
					<Container>
						<Cell row className='u-alignItemsStretch'>
							<Cell md={6} sm={12} className='u-justifyCenter'>
								<ProjectDescription title='Présentation keynote' bottom right>
									Création de visuels et mise en forme / gestion des transitions
									pour une présentation keynote
								</ProjectDescription>
							</Cell>
							<Cell md={6} sm={12} className='u-justifyCenter'>
								<Picture
									className='u-marginY0'
									src={PolymagineContent.Polymagine_Prez_Lissac01}
									notanimated
								/>
							</Cell>
							<Cell md={6} sm={12} className='u-justifyCenter'>
								<Picture
									className='u-marginY0'
									src={PolymagineContent.Polymagine_Prez_Lissac02}
									notanimated
								/>
							</Cell>
							<Cell md={6} sm={12} className='u-justifyCenter'>
								<Picture
									className='u-marginY0'
									src={PolymagineContent.Polymagine_Prez_Lissac03}
									notanimated
								/>
							</Cell>
							<Cell md={6} sm={12} className='u-justifyCenter'>
								<Picture
									className='u-marginY0'
									src={PolymagineContent.Polymagine_Prez_Lissac04}
									notanimated
								/>
							</Cell>
							<Cell md={6} sm={12} className='u-justifyCenter'>
								<Picture
									className='u-marginY0'
									src={PolymagineContent.Polymagine_Prez_Lissac05}
									notanimated
								/>
							</Cell>
							<Cell md={6} sm={12} className='u-justifyCenter'>
								<Picture
									className='u-marginY0'
									src={PolymagineContent.Polymagine_Prez_Lissac06}
									notanimated
								/>
							</Cell>
							<Cell md={6} sm={12} className='u-justifyCenter'>
								<Box style={{ position: 'relative' }}>
									<Picture
										className='u-marginY0 animation_fade animation_loop'
										style={prezLissacOverlay}
										src={PolymagineContent.Polymagine_Prez_Lissac07_1}
										notanimated
									/>
									<Picture
										className='u-marginY0'
										src={PolymagineContent.Polymagine_Prez_Lissac07_2}
										notanimated
									/>
								</Box>
							</Cell>
						</Cell>
					</Container>
				</Container>
			</Loading>

			<Loading lazyLoadHeight={827} images={PolymagineContent.sectionWebsiteIcons}>
				<Container>
					<Cell row>
						<Cell lg={6} md={8} sm={12} className='u-justifyCenter'>
							<PicturesRevealStrips pictureMain={PolymagineContent.Polymagine_Website_Icons} />
						</Cell>
						<Cell lg={6} md={4} sm={12}>
							<ProjectDescription title='Icônes - Illustration vectorielle' center>
								Création d’icônes et d’illustrations vectorielle en vue de les
								utiliser dans le site vitrine
							</ProjectDescription>
						</Cell>
					</Cell>
				</Container>
			</Loading>

			<Loading lazyLoadHeight={2946} images={PolymagineContent.sectionWebsite}>
				<Container
					fluid
					className='background_colorPolymagineBrandPrimary background_fadeApparition'
				>
					<Container>
						<Cell row className='u-alignItemsStretch'>
							<Cell lg={4} md={6} sm={12} className='u-alignItemsCenter'>
								<StickyComponent>
									<Picture
										className='u-borderedPictures u-shadow6 u-marginTop2 u-marginBottom1'
										src={PolymagineContent.Polymagine_Website_Screen_Index}
										notanimated
									/>
									<Picture
										className='u-borderedPictures u-shadow6 u-marginY1'
										src={PolymagineContent.Polymagine_Website_Screen_Contact}
										notanimated
									/>
									<ProjectDescription
										title='Webdesign - Intégration'
										center
										right
										className='u-marginTop2'
									>
										Création et intégration des mockups du site vitrine
										Polymagine
									</ProjectDescription>
								</StickyComponent>
							</Cell>
							<Cell lg={8} md={6} sm={12} className='u-justifyCenter'>
								<Picture
									className='u-borderedPictures u-shadow6 u-marginY2'
									src={PolymagineContent.Polymagine_Website_Screen_Technologie}
									notanimated
								/>
								<Picture
									className='u-borderedPictures u-shadow6 u-marginY2'
									src={PolymagineContent.Polymagine_Website_Screen_Dreamup}
									notanimated
								/>
							</Cell>
						</Cell>
					</Container>
				</Container>
			</Loading>
		</ProjectPresentation>
	);
}
