import React from 'react';

import { ProjectPresentation } from 'layouts/index';
import {
	AnimatedBackgoundPicture,
	Cell,
	Container,
	FrameBorder,
	Loading,
	Picture,
	ProjectDescription,
	StickyComponent,
	Typography
} from 'components/index';
import * as VialleContent from 'utils/pagesContent/vialle';

export default function PageVialle(){
	function VialleDescription(){
		return (
			<React.Fragment>
				<Typography variant='body1' color='secondary'>
					Sylvain Vialle est un Lunettier désireux de proposer des modèles originaux et
					adaptés aux mesures de ses clients. C’est donc tout naturellement qu’il fut l’un
					des premiers à rejoindre l’aventure DreamUp 3D - Eyewear.
				</Typography>
				<Typography variant='body1' color='secondary'>
					Afin de mettre en avant cette singularité, une toute nouvelle identité visuelle
					a été développée ainsi qu’un site web mettant en avant son histoire et ses
					modèles atypiques.
				</Typography>
				<Typography variant='body1' color='secondary'>
					Différents supports de communication personnels mais aussi transverses avec la
					plate-forme DreamUp 3D - Eyewear ont été élaborés afin de souligner ce
					partenariat.
				</Typography>
				<br />
				<Typography variant='body1' color='secondary'>
					<b>Domaines abordés :</b> Identité visuelle, Visuels d’illustration, Print,
					Webdesign.
				</Typography>
				<br />
				<Typography variant='body1' color='secondary'>
					Période de réalisation en <b>2018</b>
				</Typography>
				<Typography variant='body1' color='secondary'>
					Projet mené au sein de l’entreprise <b>Polymagine</b>
				</Typography>
			</React.Fragment>
		);
	}

	return (
		<ProjectPresentation
			projectTitle='Lunetterie Vialle'
			projectNumber={3}
			prevProject='dreamup'
			nextProject='polymagine'
			sectionPrez={VialleContent.sectionPrez}
			projectDescription={VialleDescription()}
			projectSamplePicture={VialleContent.SVialle_Charte}
			projectSampleTitle='Identité visuelle'
			projectSampleDescription='Création du logo S. Vialle et définition de la charte graphique'
		>
			<Container>
				<Cell row>
					<Loading lazyLoadHeight={1046} images={VialleContent.sectionPrint}>
						<FrameBorder animated>
							<Cell row>
								<Cell md={9}>
									<AnimatedBackgoundPicture picture={VialleContent.SVialle_CdV} />
								</Cell>
								<Cell md={3}>
									<ProjectDescription title='Print' top>
										Création de cartes de visites
									</ProjectDescription>
								</Cell>
							</Cell>
							<Cell row>
								<Cell md={3}>
									<ProjectDescription title='Visuels d’illustration' right bottom>
										Création d’une bannière pour le site web
									</ProjectDescription>
								</Cell>
								<Cell md={9}>
									<Picture src={VialleContent.SVialle_WebSite_Fond_Banner} />
								</Cell>
							</Cell>
						</FrameBorder>
					</Loading>
				</Cell>
			</Container>
			<Loading lazyLoadHeight={3533} images={VialleContent.sectionWeb}>
				<Container fluid className='background_colorGrayLight background_fadeApparition'>
					<Container>
						<Cell row className='u-alignItemsStretch'>
							<Cell md={6} sm={12} className='u-paddingY3 u-paddingX5'>
								<StickyComponent>
									<Picture
										className='u-shadow6 u-marginY1'
										src={VialleContent.SVialle_WebSite_Screen_Index}
										notanimated
									/>
									<ProjectDescription
										title='Webdesign - Intégration'
										center
										right
										className='u-marginTop2'
									>
										Création et intégration des mockups du site vitrine
										Polymagine
									</ProjectDescription>
								</StickyComponent>
							</Cell>
							<Cell md={6} sm={12} className='u-paddingY3 u-paddingX5'>
								<Picture
									className='u-shadow6 u-marginY1'
									src={VialleContent.SVialle_WebSite_Screen_Histoire}
									notanimated
								/>
							</Cell>
							<Cell md={6} sm={12} className='u-paddingY3 u-paddingX5'>
								<Picture
									className='u-shadow6 u-marginY1'
									src={VialleContent.SVialle_WebSite_Screen_Partner}
									notanimated
								/>
							</Cell>
							<Cell md={6} sm={12} className='u-paddingY3 u-paddingX5'>
								<StickyComponent>
									<Picture
										className='u-shadow6 u-marginY1'
										src={VialleContent.SVialle_WebSite_Screen_Contact}
										notanimated
									/>
								</StickyComponent>
							</Cell>
							<Cell md={6} sm={12} className='u-paddingY3 u-paddingX5'>
								<Picture
									className='u-shadow6 u-marginY1'
									src={VialleContent.SVialle_WebSite_Screen_Collections}
								/>
							</Cell>
							<Cell md={6} sm={12} className='u-paddingY3 u-paddingX5'>
								<Picture
									className='u-shadow6 u-marginY1'
									src={VialleContent.SVialle_WebSite_Screen_Collections_Modale}
								/>
							</Cell>
						</Cell>
					</Container>
				</Container>
			</Loading>
		</ProjectPresentation>
	);
}
