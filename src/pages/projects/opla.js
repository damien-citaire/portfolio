import React from 'react';
import LazyLoad from 'react-lazyload';

import { ProjectPresentation } from 'layouts/index';
import {
	AnimatedBackgoundPictureOffsetBob,
	Box,
	Cell,
	Container,
	FrameBorder,
	Loading,
	Picture,
	ProjectDescription,
	StickyComponent,
	Typography,
	Video
} from 'components/index';
import * as OplaContent from 'utils/pagesContent/opla';
import HappyNewYear from 'assets/videos/Opla_Video_HappyNewYear.mp4';
import XMas from 'assets/videos/Opla_Video_XMas.mp4';

const PageOpla = () => {
	function OplaDescription(){
		return (
			<React.Fragment>
				<Typography variant='body1' color='secondary'>
					Opla.ai propose un outil de création d’assistant conversationnel virtuel ou
					chatbot ayant pour volonté d’être accessible à tous et sans compétence
					nécessaire en code.
				</Typography>
				<Typography variant='body1' color='secondary'>
					Ayant pour ambition d’être indépendant des Gafa, cette soution propose également
					son propre moteur de Natural Language Processing et permet d’exporter son
					chatbot pour différentes plate-formes : Slack, Wordpress, Facebook Messenger...
				</Typography>
				<Typography variant='body1' color='secondary'>
					L’accompagnement dans la création de supports de communication web et print
					ainsi que la participation à l’élaboration de l’interface du builder composent
					ce projet.
				</Typography>
				<br />
				<Typography variant='body1' color='secondary'>
					<b>Domaines abordés :</b> Illustration vectorielle, Webdesign, Design d’interface,
					Communication Web & Print, Motion design...
				</Typography>
				<br />
				<Typography variant='body1' color='secondary'>
					Période de réalisation en <b>2019</b>
				</Typography>
				<Typography variant='body1' color='secondary'>
					Projet mené au sein de l’entreprise <b>CWB</b>
				</Typography>
			</React.Fragment>
		);
	}

	return (
		<ProjectPresentation
			projectTitle='Opla'
			projectNumber={5}
			prevProject='polymagine'
			sectionPrez={OplaContent.sectionPrez}
			projectDescription={OplaDescription()}
			projectSamplePicture={OplaContent.Opla_Cdv}
			projectSampleTitle='Print'
			projectSampleDescription='Création de cartes de visite pour chacuns des membres de l’équipe'
			noShadow
		>
			<LazyLoad height={873} offset={-200}>
				<Container fluid className='background_video background_fadeApparition'>
					<Container className='u-paddingBottom5'>
						<Cell row>
							<Cell>
								<ProjectDescription
									title='Motion design'
									className='u-marginBottom5'
									top
								>
									Réalisation de clips vidéos en animation visant à souhaiter les
									voeux pour les fêtes de fin d'année sur les réseaux sociaux
								</ProjectDescription>
							</Cell>
							<Cell lg={1} md={12} />
							<Cell lg={4} md={5} sm={12}>
								<Video src={XMas} controls />
							</Cell>
							<Cell lg={2} md={2} />
							<Cell lg={4} md={5} sm={12}>
								<Video src={HappyNewYear} controls />
							</Cell>
							<Cell lg={1} md={12} />
						</Cell>
					</Container>
				</Container>
			</LazyLoad>

			<Container>
				<Cell row>
					<FrameBorder animated>
						<Loading lazyLoadHeight={807} images={OplaContent.sectionBobs}>
							<Cell row>
								<Cell>
									<ProjectDescription title='Illustration vectorielle'>
										Réalisation d’illustrations vectorielles déclinant la
										mascotte existante afin d’illustrer les différentes sections
										du site web
									</ProjectDescription>
								</Cell>
								<Cell className='u-justifyCenter'>
									<Cell lg={3} md={4} sm={6} className='u-padding4'>
										<Picture src={OplaContent.Opla_Bobs_Assistant} />
									</Cell>
									<Cell lg={3} md={4} sm={6} className='u-padding4'>
										<Picture src={OplaContent.Opla_Bobs_Casdusage} />
									</Cell>
									<Cell lg={3} md={4} sm={6} className='u-padding4'>
										<Picture src={OplaContent.Opla_Bobs_Technology} />
									</Cell>
									<Cell lg={3} md={4} sm={6} className='u-padding4'>
										<Picture src={OplaContent.Opla_Bobs_ProcessusProjet} />
									</Cell>
									<Cell lg={3} md={4} sm={6} className='u-padding4'>
										<Picture src={OplaContent.Opla_Bobs_Diversity} />
									</Cell>
									<Cell lg={3} md={4} sm={6} className='u-padding4'>
										<Picture src={OplaContent.Opla_Bobs_NLP} />
									</Cell>
									<Cell lg={3} md={4} sm={6} className='u-padding4'>
										<Picture src={OplaContent.Opla_Bobs_Cloud_Premise} />
									</Cell>
									<Cell lg={3} md={4} sm={6} className='u-padding4'>
										<Picture src={OplaContent.Opla_Bobs_Hiring} />
									</Cell>
								</Cell>
							</Cell>
						</Loading>
					</FrameBorder>
				</Cell>
			</Container>

			<Loading lazyLoadHeight={2159} images={OplaContent.sectionWebsite}>
				<Container fluid className='background_paralaxOplaBlue background_fadeApparition'>
					<Container>
						<Cell row className='u-alignItemsStretch'>
							<Cell lg={4} md={4} sm={12}>
								<StickyComponent>
									<Box className='u-paddingTop4'>
										<ProjectDescription
											title='Webdesign - Intégration'
											top
											right
										>
											Création des Mockups et intégration du site vitrine Opla
										</ProjectDescription>
									</Box>
								</StickyComponent>
							</Cell>
							<Cell lg={6} md={8} sm={12} className='u-alignItemsCenter'>
								<Picture
									className='u-borderedPictures u-shadow6'
									src={OplaContent.Opla_Website}
									notanimated
								/>
							</Cell>
							<Cell lg={2} md={12} />
						</Cell>
					</Container>
				</Container>
			</Loading>

			<Container>
				<Cell row>
					<Loading lazyLoadHeight={1509} images={OplaContent.sectionOplaWorld}>
						<Cell row>
							<Cell>
								<ProjectDescription
									title='Illustration vectorielle'
									className='u-marginBottom4'
								>
									Déclinaison et mise en couleur de personnages et d’éléments
									visants à composer une illustration destinée aux réseaux sociaux
								</ProjectDescription>
							</Cell>
							<Cell disablePadding className='u-justifyCenter'>
								<Cell md={3} sm={4} xs={6}>
									<AnimatedBackgoundPictureOffsetBob
										color='orange'
										className='animatedBackgoundPictureOffsetBob_duo'
										picture={OplaContent.Opla_OplaWorld_BobDuo}
									/>
								</Cell>
								<Cell md={3} sm={4} xs={6}>
									<AnimatedBackgoundPictureOffsetBob
										color='purple'
										className='animatedBackgoundPictureOffsetBob_scared'
										picture={OplaContent.Opla_OplaWorld_BobScared}
									/>
								</Cell>
								<Cell md={3} sm={4} xs={6}>
									<AnimatedBackgoundPictureOffsetBob
										color='red'
										className='animatedBackgoundPictureOffsetBob_levier'
										picture={OplaContent.Opla_OplaWorld_BobLevier}
									/>
								</Cell>
								<Cell md={3} sm={4} xs={6}>
									<AnimatedBackgoundPictureOffsetBob
										color='blue'
										className='animatedBackgoundPictureOffsetBob_autel'
										picture={OplaContent.Opla_OplaWorld_Autel}
									/>
								</Cell>
								<Cell md={3} sm={4} xs={6}>
									<AnimatedBackgoundPictureOffsetBob
										color='purple'
										className='animatedBackgoundPictureOffsetBob_glassesBloc'
										picture={OplaContent.Opla_OplaWorld_GlassesBlocs}
									/>
								</Cell>
								<Cell md={3} sm={4} xs={6}>
									<AnimatedBackgoundPictureOffsetBob
										color='orange'
										className='animatedBackgoundPictureOffsetBob_cylinderLava'
										picture={OplaContent.Opla_OplaWorld_CylinderLava}
									/>
								</Cell>
								<Cell md={3} sm={4} xs={6}>
									<AnimatedBackgoundPictureOffsetBob
										color='blue'
										className='animatedBackgoundPictureOffsetBob_flowchart'
										picture={OplaContent.Opla_OplaWorld_FlowchartBlocs}
									/>
								</Cell>
								<Cell md={3} sm={4} xs={6}>
									<AnimatedBackgoundPictureOffsetBob
										color='red'
										className='animatedBackgoundPictureOffsetBob_computer'
										picture={OplaContent.Opla_OplaWorld_Computer}
									/>
								</Cell>
							</Cell>
						</Cell>
						<Cell row>
							<Cell>
								<ProjectDescription
									title='Illustration vectorielle - Communication web'
									right
								>
									Elaboration d’une illustration vectorielle présentant de manière
									ludique la solution et déstinée à être déclinée pour les
									différents réseaux sociaux
								</ProjectDescription>
							</Cell>
							<Cell className='u-alignItemsCenter u-justifyCenter'>
								<Picture src={OplaContent.Opla_OplaWorld} />
							</Cell>
						</Cell>
					</Loading>
				</Cell>
			</Container>

			<Loading lazyLoadHeight={1885} images={OplaContent.sectionAppScreen}>
				<Container fluid className='background_colorOplaPurple background_fadeApparition'>
					<Container>
						<Cell row>
							<Cell lg={4} md={6} className='u-marginTopAuto' disablePadding>
								<Cell disablePadding>
									<Cell>
										<ProjectDescription
											title='Webdesign - Intégration'
											top
											right
										>
											Refonte et intégration de l’interface de l’application
											en suivant les préconisations de Google via son système
											de Material Design. Aperçus de différents écrans de
											l’application
										</ProjectDescription>
									</Cell>
									<Cell>
										<Picture src={OplaContent.Opla_App_Screens_EntityDetail} />
									</Cell>
									<Cell>
										<Picture src={OplaContent.Opla_App_Screens_EditLocalEntity} />
									</Cell>
								</Cell>
							</Cell>
							<Cell lg={8} md={6} disablePadding>
								<Cell>
									<Picture src={OplaContent.Opla_App_Screens_Sign} />
								</Cell>
								<Cell lg={6} md={12}>
									<Picture src={OplaContent.Opla_App_Screens_ExtensionsList} />
								</Cell>
								<Cell lg={6} md={12}>
									<Picture src={OplaContent.Opla_App_Screens_Builder} />
								</Cell>
							</Cell>
							<Cell lg={2} disablePadding />
							<Cell lg={6} disablePadding>
								<Cell lg={12} md={6}>
									<Picture src={OplaContent.Opla_App_Screens_UsersList} />
								</Cell>
								<Cell lg={12} md={6}>
									<Picture src={OplaContent.Opla_App_Screens_AddBotDialog} />
								</Cell>
							</Cell>
							<Cell lg={4} className='u-alignItemsFlexStart' disablePadding>
								<Cell disablePadding>
									<Cell lg={12} md={6}>
										<Picture src={OplaContent.Opla_App_Screens_Flowchart} />
									</Cell>
									<Cell lg={12} md={6}>
										<Picture src={OplaContent.Opla_App_Screens_Flowchart_HGOpen} />
									</Cell>
									<Cell lg={12} md={6}>
										<Picture src={OplaContent.Opla_App_Screens_Flowchart_Edit} />
									</Cell>
								</Cell>
							</Cell>
						</Cell>
					</Container>
				</Container>
			</Loading>
		</ProjectPresentation>
	);
};

export default PageOpla;
