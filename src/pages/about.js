import React, { useState, useEffect } from 'react';

import {
	AboutInformation,
	AboutSection,
	AnimatedBackgoundPicture,
	Box,
	Container,
	Cell,
	FrameBorder,
	Spinner,
	StickyComponent,
	Typography
} from 'components/index';
import * as AboutContent from 'utils/pagesContent/about';
import 'assets/style/pages_specific/about.scss';

const PageAbout = () => {
	const [
		imgsLoaded,
		setImgsLoaded
	] = useState(false);

	useEffect(() => {
		const $imagesPrez = AboutContent.images;

		const loadImage = (image) => {
			return new Promise((resolve, reject) => {
				const loadImg = new Image();
				loadImg.src = image;
				loadImg.onload = () => resolve(image);
				loadImg.onerror = (err) => reject(err);
			});
		};

		Promise.all($imagesPrez.map((image) => loadImage(image)))
			.then(() => setImgsLoaded(true))
			.catch((err) => console.log('Failed to load images', err));
	});

	return (
		<React.Fragment>
			{
				imgsLoaded ? <React.Fragment>
					<Container className='about'>
						<Cell row disablePadding className='about-header'>
							<Cell md={4} sm={10} xs={12} className='u-alignSelfStretch u-justifyFlexStart'>
								<AnimatedBackgoundPicture
									picture={AboutContent.Prez_ProfilPict}
									className='about-profilePict'
								/>
							</Cell>
							<Cell
								lg={7}
								md={8}
								className='u-justifyCenter u-paddingLeft2'
								dirColumn
							>
								<Cell disablePadding className='about-headerTranslateWrapper'>
									<Box className='about-headerTranslateComponent'>
										<Typography
											variant='h4'
											color='secondary'
											className='about-name'
										>
											Damien Citaire
										</Typography>
									</Box>
								</Cell>
								<Cell disablePadding className='about-headerTranslateWrapper'>
									<Box className='about-headerTranslateComponent'>
										<Typography
											variant='h5'
											color='primary'
											className='about-post'
										>
											Développeur Full Stack - Designer
										</Typography>
									</Box>
								</Cell>
								<Cell disablePadding className='about-headerTranslateWrapper'>
									<Box className='about-headerTranslateComponent'>
										<Cell lg={4} md={6} sm={4} xs={6} disablePaddingX>
											<AboutInformation
												text='06 76 79 34 13'
												icon={AboutContent.Prez_CV_Phone}
											/>
										</Cell>
										<Cell lg={4} md={6} sm={4} xs={6} disablePaddingX>
											<AboutInformation
												text='63530 Sayat'
												icon={AboutContent.Prez_CV_Home}
											/>
										</Cell>
										<Cell lg={4} md={6} sm={4} xs={6} disablePaddingX>
											<AboutInformation
												text='Gitlab'
												link='https://gitlab.com/damien-citaire'
												icon={AboutContent.Prez_CV_Gitlab}
											/>
										</Cell>
										<Cell lg={4} md={6} sm={4} xs={6} disablePaddingX>
											<AboutInformation
												text='damien.citaire@gmail.com'
												icon={AboutContent.Prez_CV_Mail}
											/>
										</Cell>
										<Cell lg={4} md={6} sm={4} xs={6} disablePaddingX>
											<AboutInformation
												text='Permis B + Véhicule'
												icon={AboutContent.Prez_CV_Car}
											/>
										</Cell>
										<Cell lg={4} md={6} sm={4} xs={6} disablePaddingX>
											<AboutInformation
												text='Linkedin'
												link='https://www.linkedin.com/in/damien-citaire-41221648/'
												icon={AboutContent.Prez_CV_Linkedin}
											/>
										</Cell>
										<Cell
											className='u-marginTop1 about-downloadSection about-downloadSection_top'
											disablePadding
										>
											<Cell lg={4} md={6} sm={4} xs={6} disablePaddingX>
												<a
													className='about-downloadItem about-downloadItem_CV'
													href={
														process.env.PUBLIC_URL +
														'/CV_Damien-Citaire.pdf'
													}
													download
												>
													<Box className='about-downloadIcon' />
													<Typography
														variant='caption1'
														color='secondary'
														className='about-downloadLabel'
													>
														Télécharger le C.V.
													</Typography>
												</a>
											</Cell>
											<Cell lg={4} md={6} sm={4} xs={6} disablePaddingX>
												<a
													className='about-downloadItem about-downloadItem_Portfolio'
													href={
														process.env.PUBLIC_URL +
														'/Portfolio_Damien-Citaire.pdf'
													}
													download
												>
													<Box className='about-downloadIcon' />
													<Typography
														variant='caption1'
														color='secondary'
														className='about-downloadLabel'
													>
														Télécharger le Portfolio
													</Typography>
												</a>
											</Cell>
										</Cell>
									</Box>
								</Cell>
							</Cell>
						</Cell>

						<Cell row className='about-body' disablePaddingY>
							<FrameBorder animated style={{ pointerEvents: 'auto' }}>
								<Cell
									row
									className='about-bodyInformations'
								>
									<Cell md={4} dirColumn className='u-justifyFlexStart'>
										<StickyComponent>
											<Cell disablePadding className='u-paddingTop4 u-alignItemsStretch'>
												<Typography variant='h5' color='secondary'>
													Outils et technologies
												</Typography>
												<Cell className='u-marginTop2'>
													<Typography variant='subtitle1' color='primary'>
														Design et prototypage
													</Typography>
												</Cell>
												<Cell lg={8} md={12} sm={8} xs={8}>
													<AnimatedBackgoundPicture
														picture={
															AboutContent.Prez_SkillsLogo_Sketch
														}
														className='about-iconDesignMain'
													/>
												</Cell>
												<Cell lg={4} md={12} sm={4} xs={4}>
													<Cell lg={12} md={6} sm={12} disablePadding className='u-alignItemsStretch u-alignSelfStretch'>
														<AnimatedBackgoundPicture
															picture={
																AboutContent.Prez_SkillsLogo_MaterialDesign
															}
															className='about-iconDesignSecondary'
														/>
													</Cell>
													<Cell lg={12} md={6} sm={12} disablePadding className='u-alignItemsStretch u-alignSelfStretch'>
														<AnimatedBackgoundPicture
															picture={
																AboutContent.Prez_SkillsLogo_Invision
															}
															className='about-iconDesignSecondary'
														/>
													</Cell>
												</Cell>
												<Cell>
													<Cell
														lg={3}
														md={6}
														sm={3}
														xs={3}
														disablePadding
													>
														<AnimatedBackgoundPicture
															picture={
																AboutContent.Prez_SkillsLogo_AdobePs
															}
															className='about-iconDesignThird'
														/>
													</Cell>
													<Cell
														lg={3}
														md={6}
														sm={3}
														xs={3}
														disablePadding
													>
														<AnimatedBackgoundPicture
															picture={
																AboutContent.Prez_SkillsLogo_AdobeAi
															}
															className='about-iconDesignThird'
														/>
													</Cell>
													<Cell
														lg={3}
														md={6}
														sm={3}
														xs={3}
														disablePadding
													>
														<AnimatedBackgoundPicture
															picture={
																AboutContent.Prez_SkillsLogo_AdobeId
															}
															className='about-iconDesignThird'
														/>
													</Cell>
													<Cell
														lg={3}
														md={6}
														sm={3}
														xs={3}
														disablePadding
													>
														<AnimatedBackgoundPicture
															picture={
																AboutContent.Prez_SkillsLogo_AdobeAe
															}
															className='about-iconDesignThird'
														/>
													</Cell>
												</Cell>
												<Cell className='u-marginTop2'>
													<Typography variant='subtitle1' color='primary'>
														Développement front et back
													</Typography>
												</Cell>
												<Cell disablePadding className='u-alignItemsStretch'>
													<Cell lg={4} md={12} sm={4} xs={4}>
														<Cell lg={12} md={6} disablePadding className='u-alignItemsStretch u-alignSelfStretch'>
															<AnimatedBackgoundPicture
																picture={
																	AboutContent.Prez_SkillsLogo_HTML5
																}
																className='about-iconDevMain'
															/>
														</Cell>
														<Cell lg={12} md={6} disablePadding className='u-alignItemsStretch u-alignSelfStretch'>
															<AnimatedBackgoundPicture
																picture={
																	AboutContent.Prez_SkillsLogo_CSS3
																}
																className='about-iconDevMain'
															/>
														</Cell>
														<Cell lg={12} md={6} disablePadding className='u-alignItemsStretch u-alignSelfStretch'>
															<AnimatedBackgoundPicture
																picture={
																	AboutContent.Prez_SkillsLogo_PHP
																}
																className='about-iconDevMain'
															/>
														</Cell>
														<Cell lg={12} md={6} disablePadding className='u-alignItemsStretch u-alignSelfStretch'>
															<AnimatedBackgoundPicture
																picture={
																	AboutContent.Prez_SkillsLogo_JS
																}
																className='about-iconDevMain'
															/>
														</Cell>
													</Cell>
													<Cell lg={8} md={12} sm={8} xs={8}>
														<Cell xs={6} disablePadding>
															<AnimatedBackgoundPicture
																picture={
																	AboutContent.Prez_SkillsLogo_Sass
																}
																className='about-iconDevSecondary'
															/>
														</Cell>
														<Cell xs={6} disablePadding>
															<AnimatedBackgoundPicture
																picture={
																	AboutContent.Prez_SkillsLogo_Wordpress
																}
																className='about-iconDevSecondary'
															/>
														</Cell>
														<Cell xs={6} disablePadding>
															<AnimatedBackgoundPicture
																picture={
																	AboutContent.Prez_SkillsLogo_Bootstrap
																}
																className='about-iconDevSecondary'
															/>
														</Cell>
														<Cell xs={6} disablePadding>
															<AnimatedBackgoundPicture
																picture={
																	AboutContent.Prez_SkillsLogo_Mui
																}
																className='about-iconDevSecondary'
															/>
														</Cell>
														<Cell xs={6} disablePadding>
															<AnimatedBackgoundPicture
																picture={
																	AboutContent.Prez_SkillsLogo_Symfony
																}
																className='about-iconDevSecondary'
															/>
														</Cell>
														<Cell xs={6} disablePadding>
															<AnimatedBackgoundPicture
																picture={
																	AboutContent.Prez_SkillsLogo_SQL
																}
																className='about-iconDevSecondary'
															/>
														</Cell>
														<Cell xs={6} disablePadding>
															<AnimatedBackgoundPicture
																picture={
																	AboutContent.Prez_SkillsLogo_React
																}
																className='about-iconDevSecondary'
															/>
														</Cell>
														<Cell xs={6} disablePadding>
															<AnimatedBackgoundPicture
																picture={
																	AboutContent.Prez_SkillsLogo_Git
																}
																className='about-iconDevSecondary'
															/>
														</Cell>
													</Cell>
												</Cell>
											</Cell>
											<Cell className='u-marginTop4'>
												<Typography variant='h5' color='secondary'>
													Centres d'intêret
												</Typography>
											</Cell>
											<Cell>
												<Box className='about-hobbiesSection'>
													<Cell
														lg={4}
														md={6}
														sm={4}
														xs={4}
														disablePadding
													>
														<AnimatedBackgoundPicture
															picture={
																AboutContent.Prez_Interest_Cinema
															}
															className='about-iconHobbie'
														/>
													</Cell>
													<Cell
														lg={4}
														md={6}
														sm={4}
														xs={4}
														disablePadding
													>
														<AnimatedBackgoundPicture
															picture={
																AboutContent.Prez_Interest_Sport
															}
															className='about-iconHobbie'
														/>
													</Cell>
													<Cell
														lg={4}
														md={6}
														sm={4}
														xs={4}
														disablePadding
													>
														<AnimatedBackgoundPicture
															picture={
																AboutContent.Prez_Interest_Bricolage
															}
															className='about-iconHobbie'
														/>
													</Cell>
												</Box>
											</Cell>
										</StickyComponent>
									</Cell>

									<Cell lg={7} md={8} dirColumn className='u-justifyFlexStart'>
										<StickyComponent>
											<Cell disablePadding className='u-paddingTop4'>
												<Typography variant='h5' color='secondary'>
													Expériences Professionnelles
												</Typography>
											</Cell>
											<Cell>
												<AboutSection
													title='Designer - Intégrateur Front-end'
													subtitle='CWB'
													dateBeginMonth='12'
													dateBeginYear='2018'
													dateEndMonth='01'
													dateEndYear='2020'
												>
													<Typography variant='body1' color='secondary'>
														Création de visuels dédiés au web et aux
														réseaux sociaux,
													</Typography>
													<Typography variant='body1' color='secondary'>
														Participation à l’élaboration d’une
														application de chatbots :
													</Typography>

													<Typography variant='body1' color='secondary'>
														• Création de mockups basés sur Material
														Design,
													</Typography>
													<Typography variant='body1' color='secondary'>
														• Intégration des mockups.
													</Typography>
													<Typography variant='body1' color='secondary'>
														Réalisation de clips animés pour les réseaux
														sociaux
													</Typography>
												</AboutSection>
											</Cell>
											<Cell>
												<AboutSection
													title='Designer - Intégrateur Front-end'
													subtitle='Polymagine'
													dateBeginMonth='07'
													dateBeginYear='2014'
													dateEndMonth='12'
													dateEndYear='2018'
												>
													<Typography variant='body1' color='secondary'>
														Définition d’identités visuelles et de
														chartes graphiques,
													</Typography>
													<Typography variant='body1' color='secondary'>
														Création de visuels dédiés au print et au
														web,
													</Typography>
													<Typography variant='body1' color='secondary'>
														Réalisation de clips de présentation animés,
													</Typography>
													<Typography variant='body1' color='secondary'>
														Participation à l’élaboration d’applications
														incluant un visualisateur 3D :
													</Typography>
													<Typography variant='body1' color='secondary'>
														• Réflexion sur l’UI/UX,
													</Typography>
													<Typography variant='body1' color='secondary'>
														• Design d’interface en wireframe puis en
														maquettes fidèles,
													</Typography>
													<Typography variant='body1' color='secondary'>
														• Intégration des mockups.
													</Typography>
												</AboutSection>
											</Cell>
											<Cell>
												<AboutSection
													title='Graphiste - Webdesigner'
													subtitle='Appuy Créateurs'
													dateBeginMonth='09'
													dateBeginYear='2011'
													dateEndMonth='07'
													dateEndYear='2014'
												>
													<Typography variant='body1' color='secondary'>
														Création de logos et visuels dédiés au
														print,
													</Typography>
													<Typography variant='body1' color='secondary'>
														Déploiement et installation de CMS
													</Typography>
													<Typography variant='body1' color='secondary'>
														Intégration et adaptation à la charte
														graphique de templates
													</Typography>
												</AboutSection>
											</Cell>
											<Cell disablePadding className='u-marginTop4'>
												<Typography variant='h5' color='secondary'>
													Formations
												</Typography>
											</Cell>
											<Cell>
												<AboutSection
													title='Formation Concepteur d’Applications'
													subtitle='ADREC - Clermont-Ferrand (63)'
													dateBeginMonth='09'
													dateBeginYear='2020'
													dateEndMonth='06'
													dateEndYear='2021'
												>
													<Typography variant='body1' color='secondary'>
														Diplôme de niveau 6{' '}
														<small>Formation en cours</small>
													</Typography>
												</AboutSection>
											</Cell>
											<Cell>
												<AboutSection
													title='Formation Infographiste Multimédia'
													subtitle='AFPA - Laval (53)'
													dateBeginMonth='09'
													dateBeginYear='2010'
													dateEndMonth='07'
													dateEndYear='2011'
												>
													<Typography variant='body1' color='secondary'>
														Diplôme de niveau 5
													</Typography>
												</AboutSection>
											</Cell>
											<Cell>
												<AboutSection
													title='Baccalauréat STI Arts Appliqués'
													subtitle='Lycée Raymond Loewy - La Souterraine (23)'
													dateBeginMonth='04'
													dateBeginYear='2004'
													dateEndMonth='07'
													dateEndYear='2007'
												/>
											</Cell>
										</StickyComponent>
									</Cell>
								</Cell>
							</FrameBorder>
						</Cell>
					</Container>
					<Box className='about-downloadSection about-downloadSection_bottom'>
						<a
							className='about-downloadItem about-downloadItem_CV'
							href={process.env.PUBLIC_URL + '/CV_Damien-Citaire.pdf'}
							download
						>
							<Box className='about-downloadIcon' />
							<Typography
								variant='caption1'
								color='secondary'
								className='about-downloadLabel'
							>
								Télécharger le C.V.
							</Typography>
						</a>
						<a
							className='about-downloadItem about-downloadItem_Portfolio'
							href={process.env.PUBLIC_URL + '/Portfolio_Damien-Citaire.pdf'}
							download
						>
							<Box className='about-downloadIcon' />
							<Typography
								variant='caption1'
								color='secondary'
								className='about-downloadLabel'
							>
								Télécharger le Portfolio
							</Typography>
						</a>
					</Box>
				</React.Fragment> :
				<Spinner message='Chargement du Profil' fullScreen />}
		</React.Fragment>
	);
};

export default PageAbout;
