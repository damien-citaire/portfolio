import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { Box, Picture } from '../index';

import { validatedFormatedProps } from 'utils';

import './style.scss';

const COLORS = [
	'default',
	'oodibee',
	'dreamup'
];
const COLOR_DEFAULT = 'default';

const AnimatedBackgoundPicture = (props) => {
	function colorValidator(bckgrdColor){
		let color = validatedFormatedProps(bckgrdColor, COLORS, COLOR_DEFAULT);
		return color;
	}

	let animatedBackgoundPictureClasses = classNames(
		props.className,
		'animatedBackgroundPicture',
		{ animatedBackgroundPicture_smallPadding: props.smallPadding },
		{ [`animatedBackgroundPicture_color${colorValidator(props.color)}`]: props.color }
	);

	return (
		<Box className={animatedBackgoundPictureClasses}>
			<Picture
				className='animatedBackgroundPicture-picture'
				src={props.picture}
				alt={props.alt}
			/>
		</Box>
	);
};

AnimatedBackgoundPicture.defaultProps = {
	alt: '',
	color: COLOR_DEFAULT
};

AnimatedBackgoundPicture.propTypes = {
	className: PropTypes.string,
	picture: PropTypes.string,
	alt: PropTypes.string,
	color: PropTypes.oneOf(COLORS)
};

export default AnimatedBackgoundPicture;
