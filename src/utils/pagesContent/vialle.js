//
// Import Images
// ---
import SVialle_Charte from 'assets/images/projects/SVialle_Charte.jpg';
import SVialle_CdV from 'assets/images/projects/SVialle_CdV.png';
import SVialle_WebSite_Fond_Banner from 'assets/images/projects/SVialle_WebSite_Fond_Banner.jpg';
import SVialle_WebSite_Screen_Partner from 'assets/images/projects/SVialle_WebSite_Screen_Partner.jpg';
import SVialle_WebSite_Screen_Collections_Modale from 'assets/images/projects/SVialle_WebSite_Screen_Collections_Modale.jpg';
import SVialle_WebSite_Screen_Collections from 'assets/images/projects/SVialle_WebSite_Screen_Collections.jpg';
import SVialle_WebSite_Screen_Contact from 'assets/images/projects/SVialle_WebSite_Screen_Contact.jpg';
import SVialle_WebSite_Screen_Histoire from 'assets/images/projects/SVialle_WebSite_Screen_Histoire.jpg';
import SVialle_WebSite_Screen_Index from 'assets/images/projects/SVialle_WebSite_Screen_Index.jpg';
//

//
// Stock Images in const for Loading Sections
// ---
export const sectionPrez = [
	SVialle_Charte,
];

export const sectionPrint = [
	SVialle_CdV,
	SVialle_WebSite_Fond_Banner
];

export const sectionWeb = [
	SVialle_WebSite_Screen_Index,
	SVialle_WebSite_Screen_Histoire,
	SVialle_WebSite_Screen_Partner,
	SVialle_WebSite_Screen_Contact,
	SVialle_WebSite_Screen_Collections,
	SVialle_WebSite_Screen_Collections_Modale
];
//

//
// Export Images
// ---
export {
    SVialle_Charte,
    SVialle_CdV,
    SVialle_WebSite_Fond_Banner,
    SVialle_WebSite_Screen_Partner,
    SVialle_WebSite_Screen_Collections_Modale,
    SVialle_WebSite_Screen_Collections,
    SVialle_WebSite_Screen_Contact,
    SVialle_WebSite_Screen_Histoire,
    SVialle_WebSite_Screen_Index
}
//