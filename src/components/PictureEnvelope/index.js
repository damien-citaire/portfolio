import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { Box, Picture } from '../index';

import { DreamUp_Print_Invit_Envelope } from 'utils/pagesContent/dreamup';

import './style.scss';

const PictureEnvelope = (props) => {
	let pictureEnvelopeClasses = classNames(
		props.className,
		{ pictureEnvelope_right: props.right },
		'pictureEnvelope'
	);

	return (
		<Box className={pictureEnvelopeClasses}>
			<Picture className='pictureEnvelope_envelope' src={DreamUp_Print_Invit_Envelope} />
			<Picture className='pictureEnvelope_picture' src={props.picture} />
		</Box>
	);
};

PictureEnvelope.defaultProps = {
    right: false,
};

PictureEnvelope.propTypes = {
	className: PropTypes.string,
	picture: PropTypes.string.isRequired,
	right: PropTypes.bool
};

export default PictureEnvelope;
