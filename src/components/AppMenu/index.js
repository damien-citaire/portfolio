import React, { useState } from 'react';
import onClickOutside from 'react-onclickoutside';
import { CSSTransition } from 'react-transition-group';
import { NavLink } from 'react-router-dom';
import { projectsRoutes } from 'utils/routes';
import { useLocation } from 'react-router-dom';
import classNames from 'classnames';

import { Box, Container, Typography, ScrollTopArrow } from '../index';

import './style.scss';

function AppMenu(){
	const [
		open,
		setOpen
	] = useState(false);

	const [
		openAnim,
		setOpenAnim
	] = useState(false);

	function openTimeOut(){
		setTimeout(() => {
			setOpen(!open);
		}, 275);
	}

	const toggleOpen = () =>

			open === true ? openTimeOut() :
			setOpen(!open);

	const toggleOpenAnim = () => setOpenAnim(!openAnim);

	AppMenu.handleClickOutside = () =>
		setTimeout(() => {
			setOpen(false);
		}, 275) + setOpenAnim(false);

	let location = useLocation();
	let fullLocation = location.pathname;
	let AppMenuProjectClasses = classNames('appMenu-menuItem', 'appMenu-expandButton', {
		'isActive': fullLocation.includes('project/')
	});

	return (
		<Container className='appMenu-wrapper' fluid>
			{fullLocation.includes('project/') && (
				<ScrollTopArrow className={AppMenuProjectClasses} />
			)}
			<Box className='appMenu-content'>
				<Container justify='space-around' alignItems='center' className='appMenu'>
					<NavLink
						exact
						key='home'
						to='/'
						className='appMenu-menuItem'
						activeClassName='isActive'
					>
						<Box className='appMenu-menuItemIcon appMenu-menuItemIcon_home' />
						<Typography className='appMenu-menuItemLabel' variant='caption1'>
							Accueil
						</Typography>
					</NavLink>
					<NavLink
						exact
						key='about'
						to='/about'
						className='appMenu-menuItem'
						activeClassName='isActive'
					>
						<Box className='appMenu-menuItemIcon appMenu-menuItemIcon_about' />
						<Typography className='appMenu-menuItemLabel' variant='caption1'>
							Profil
						</Typography>
					</NavLink>
					<div
						tabIndex={0}
						className={AppMenuProjectClasses}
						role='button'
						onKeyPress={() => toggleOpen(!open) + toggleOpenAnim(!openAnim)}
						onClick={() => toggleOpen(!open) + toggleOpenAnim(!openAnim)}
					>
						<Box className='appMenu-menuItemIcon appMenu-menuItemIcon_projects' />
						<Typography className='appMenu-menuItemLabel' variant='caption1'>
							Projets
						</Typography>
					</div>
				</Container>
			</Box>
			<CSSTransition
				classNames={{
					enterActive: 'appMenu-subMenu_animatedEnterActive',
					exitActive: 'appMenu-subMenu_animatedExitActive',
					exitDone: 'appMenu-subMenu_animatedExitDone'
				}}
				timeout={2500}
				in={openAnim}
			>
				<React.Fragment>
					{open && (
						<Container className='appMenu-subMenuWrapper' fluid>
							<Container className='appMenu-subMenu'>
								{projectsRoutes.map(({ path, name }) => (
									<li key={path} className='appMenu-subMenuListItem'>
										<NavLink
											exact
											key={path}
											to={path}
											activeClassName='isActive'
										>
											{name}
										</NavLink>
									</li>
								))}
							</Container>
						</Container>
					)}
				</React.Fragment>
			</CSSTransition>
		</Container>
	);
}

const clickOutsideConfig = {
	handleClickOutside: () => AppMenu.handleClickOutside
};

export default onClickOutside(AppMenu, clickOutsideConfig);
