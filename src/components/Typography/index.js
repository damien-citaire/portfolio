import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { validatedFormatedProps } from 'utils';

import './style.scss';

// Defining the HTML tag that the component will support
const VARIANTS_MAPPING = {
  h1: 'h1',
  h2: 'h2',
  h3: 'h3',
  h4: 'h4',
  h5: 'h5',
  h6: 'h6',
  subtitle1: 'h6',
  subtitle2: 'h6',
  body1: 'p',
  body2: 'p',
  body3: 'p',
  caption1: 'p',
  caption2: 'p',
  caption3: 'p',
  caption4: 'p',
  overline: 'p',
};

const TYPOGRAPHY_VARIANTS = [
  'h1',
  'h2',
  'h3',
  'h4',
  'h5',
  'h6',
  'subtitle1',
  'subtitle2',
  'body1',
  'body2',
  'body3',
  'caption1',
  'caption2',
  'caption3',
  'caption4',
  'overline',
];
const TYPOGRAPHY_DEFAULT_VARIANT = 'body1';
const TYPOGRAPHY_COLORS = ['primary', 'secondary', 'black', 'grey', 'white'];
const TYPOGRAPHY_DEFAULT_COLOR = 'primary';

const Typography = ({ variant, color, children, ...props }) => {
  // If the variant exists in VARIANTS_MAPPING, we use it. 
  // Otherwise, use p tag instead.
  let Component = variant ? VARIANTS_MAPPING[variant] : 'p';

  let typographyVariantValidator = validatedFormatedProps(variant, TYPOGRAPHY_VARIANTS, TYPOGRAPHY_DEFAULT_VARIANT);
  let typographyColorValidator = validatedFormatedProps(color, TYPOGRAPHY_COLORS, TYPOGRAPHY_DEFAULT_COLOR);

  let typographyClasses = classNames(
    props.className,
    'typography',
    { 
      [`typography_variant${typographyVariantValidator}`]: variant,
      [`typography_color${typographyColorValidator}`]: color,

    }
);

  return (
    <Component
      {...props}
      className={typographyClasses}
    >
      {children}
    </Component>
  );
};

Typography.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node.isRequired,
  variant: PropTypes.oneOf(TYPOGRAPHY_VARIANTS),
  color: PropTypes.oneOf(TYPOGRAPHY_COLORS),
};

export default Typography;