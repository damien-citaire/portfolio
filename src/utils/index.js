// ---------------
// Utils functions
// ---------------

// Validate submited props with a defined array,
// if no match return defined default prop
//
export function componentPropsValidator(props, array, defaultValue) {
  return (array.includes(props) ? props : defaultValue);
};


// Convert dash-separated string to CamelCase
//
export function camelCase(a) {
  if(typeof a === 'string' || a instanceof String) {
    return ( a || '' ).toLowerCase().replace(/(\b|-)\w/g, function(m) {
      return m.toUpperCase().replace(/-/,'');
    });
  } else {
    return a;
  }
  
}


// Combine componentPropsValidator() and camelCase()
// to export final props
//
export function validatedFormatedProps(props, array, defaultValue) {

  let propsToBeFormated = componentPropsValidator(props, array, defaultValue);
  return camelCase(propsToBeFormated);
}