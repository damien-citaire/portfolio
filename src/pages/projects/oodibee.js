import React from 'react';

import { ProjectPresentation } from 'layouts/index';
import {
	AnimatedBackgoundPicture,
	AnimatedBackgoundPictureOffset,
	Cell,
	Container,
	FrameBorder,
	Loading,
	Picture,
	ProjectDescription,
	StickyComponent,
	Typography
} from 'components/index';
import * as OodibeeContent from 'utils/pagesContent/oodibee';

export default function PageOodibee(){
	function OodibeeDescription(){
		return (
			<React.Fragment>
				<Typography variant='body1' color='secondary'>
					Projet ayant pour ambition d’apporter un souffle nouveau dans la conception de
					produits personnalisables au travers de backplates pour smartphones et de
					tableaux via une web application.
				</Typography>
				<Typography variant='body1' color='secondary'>
					Ce service offre la possibilité de choisir parmi une banque de créations
					d’artistes ou d’importer ses propres visuels qui seront traités afin d’être
					compatibles pour le procédé de gravure laser sur les essences de bois composant
					ces produits.
				</Typography>
				<Typography variant='body1' color='secondary'>
					La prévisualisation en 3D via l’outil de personnalisation des produits est le
					coeur de ce projet dont il a fallu également développer l’identité visuelle
					ainsi que tous les supports de communication associés.
				</Typography>
				<br />
				<Typography variant='body1' color='secondary'>
					<b>Domaines abordés :</b> Identité visuelle, Visuels d’illustration,
					Illustrations vectorielles, Webdesign, Design d’interface, Pictogrammes,
					Communication Web...
				</Typography>
				<br />
				<Typography variant='body1' color='secondary'>
					Période de réalisation entre <b>2014</b> & <b>2016</b>
				</Typography>
				<Typography variant='body1' color='secondary'>
					Projet mené au sein de l’entreprise <b>Polymagine</b>
				</Typography>
			</React.Fragment>
		);
	}

	return (
		<ProjectPresentation
			projectTitle='Oodibee'
			projectNumber={1}
			nextProject='dreamup'
			sectionPrez={OodibeeContent.sectionPrez}
			projectDescription={OodibeeDescription()}
			projectSamplePicture={OodibeeContent.Oodibee_Charte}
			projectSampleTitle='Identité visuelle'
			projectSampleDescription='Création du logo Oodibee et définition de la charte graphique'
		>
			<Container>
				<Cell row>
					<FrameBorder animated>
						<Loading lazyLoadHeight={834} images={OodibeeContent.sectionVisuelsPrez}>
							<Cell row className='u-alignItemsStretch'>
								<Cell disablePaddingY>
									<ProjectDescription title='Visuels d’illustration'>
										Création de visuels de présentation des produits de la
										marque Oodibee
									</ProjectDescription>
								</Cell>
								<Cell md={4} sm={6}>
									<AnimatedBackgoundPicture
										className='u-alignSelfStretch'
										picture={OodibeeContent.Oodibee_PrezProduit_Bumper}
									/>
								</Cell>
								<Cell md={8} sm={6} disablePadding>
									<Cell md={12} sm={12}>
										<AnimatedBackgoundPicture
											picture={OodibeeContent.Oodibee_PrezProduit_Smartphones}
										/>
									</Cell>
									<Cell md={8} sm={12}>
										<AnimatedBackgoundPicture
											picture={OodibeeContent.Oodibee_PrezProduit_Tableaux}
										/>
									</Cell>
									<Cell md={4} sm={12} className='u-alignSelfStretch'>
										<AnimatedBackgoundPicture
										 	className='u-alignSelfStretch'
											picture={OodibeeContent.Oodibee_PrezProduit_Plaques}
										/>
									</Cell>
								</Cell>
							</Cell>
						</Loading>

						<Loading lazyLoadHeight={604} images={OodibeeContent.sectionVisuelsIndic}>
							<Cell row className='u-justifyCenter'>
								<Cell xl={8} lg={10} disablePaddingX className='u-alignItemsStretch'>
									<Cell sm={6} className='u-alignItemsStretch'> 
										<AnimatedBackgoundPictureOffset
											className='animatedBackgroundPictureOffset_first'
											picture={OodibeeContent.Oodibee_Indications_Bumper}
										/>
									</Cell>
									<Cell sm={6} className='u-alignItemsStretch'> 
										<AnimatedBackgoundPictureOffset
											className='animatedBackgroundPictureOffset_second'
											picture={OodibeeContent.Oodibee_Indications_Dibon}
										/>
									</Cell>
									<Cell sm={6} className='u-alignItemsStretch'> 
										<AnimatedBackgoundPictureOffset
											className='animatedBackgroundPictureOffset_third'
											picture={OodibeeContent.Oodibee_Indications_Scotch}
										/>
									</Cell>
									<Cell sm={6} className='u-alignItemsStretch'> 
										<AnimatedBackgoundPictureOffset
											className='animatedBackgroundPictureOffset_fourth'
											picture={OodibeeContent.Oodibee_Indications_Essences_Dibond}
										/>
									</Cell>
								</Cell>
								<Cell xl={4} lg={12} className='u-paddingTop4'>
									<ProjectDescription title='Visuels d’illustration' bottom>
										Création de visuels d’explication d’utilisation et de
										composition des produits de la marque Oodibee
									</ProjectDescription>
								</Cell>
							</Cell>
						</Loading>

						<Loading lazyLoadHeight={237} images={OodibeeContent.sectionVisuelsAlveoles}>
							<Cell row className='u-alignItemsCenterh'>
								<Cell md={6}>
									<ProjectDescription title='Visuels d’illustration' center right>
										Représentation des essences de bois disponibles sous forme
										d’alvéoles
									</ProjectDescription>
								</Cell>
								<Cell md={6} className='u-justifyCenter'>
									<Picture src={OodibeeContent.Oodibee_Vignette_Essence_Acajou} />
									<Picture src={OodibeeContent.Oodibee_Vignette_Essence_Ebene} />
									<Picture src={OodibeeContent.Oodibee_Vignette_Essence_Evene} />
									<Picture src={OodibeeContent.Oodibee_Vignette_Essence_Movengi} />
									<Picture src={OodibeeContent.Oodibee_Vignette_Essence_Poirier} />
								</Cell>
							</Cell>
						</Loading>

						<Loading lazyLoadHeight={1104} images={OodibeeContent.sectionCreaPerso}>
							<Cell row className='u-alignItemsFlexEnd'>
								<Cell md={6} sm={12} disablePaddingX>
									<Cell xs={6}>
										<Picture src={OodibeeContent.Oodibee_Crea_Perso_Cat} />
									</Cell>
									<Cell xs={6}>
										<Picture src={OodibeeContent.Oodibee_Crea_Perso_Eagle} />
									</Cell>
									<Cell xs={6}>
										<Picture src={OodibeeContent.Oodibee_Crea_Perso_Escape} />
									</Cell>
									<Cell xs={6}>
										<Picture src={OodibeeContent.Oodibee_Crea_Perso_Kiss} />
									</Cell>
								</Cell>
								<Cell md={6} sm={12} disablePaddingX>
									<Cell lg={8} md={12}>
										<ProjectDescription
											title='Illustration vectorielle'
											center
											left
										>
											Création d’illustrations vectorielles en vue d’alimenter
											le catalogue de créations de la boutique en ligne
										</ProjectDescription>
									</Cell>
									<Cell md={12} disablePadding>
										<Cell lg={4} md={6} sm={4} xs={6}>
											<Picture src={OodibeeContent.Oodibee_Crea_Perso_Preview_Eagle} />
										</Cell>
										<Cell lg={4} md={6} sm={4} xs={6}>
											<Picture src={OodibeeContent.Oodibee_Crea_Perso_Preview_Escape} />
										</Cell>
										<Cell lg={4} md={6} sm={4} xs={6}>
											<Picture src={OodibeeContent.Oodibee_Crea_Perso_Preview_Kiss} />
										</Cell>
										<Cell lg={4} md={6} sm={4} xs={6}>
											<Picture src={OodibeeContent.Oodibee_Crea_Perso_Preview_Cat} />
										</Cell>
									</Cell>
								</Cell>
							</Cell>
						</Loading>
					</FrameBorder>
				</Cell>
			</Container>

			<Loading lazyLoadHeight={2392} images={OodibeeContent.sectionWebsite}>
				<Container
					fluid
					className='background_colorOodibeeBrandSecondary background_fadeApparition'
				>
					<Container>
						<Cell row className='u-alignItemsStretch'>
							<Cell md={6} sm={12} className='u-alignItemsCenter'>
								<ProjectDescription title='Webdesign - Intégration' center left>
									Création des Mockups du Site Marchand Oodibee Page Bumper iPhone
									5-5s
								</ProjectDescription>
								<Picture
									className='u-borderedPictures u-shadow6 u-marginY3'
									src={OodibeeContent.Oodibee_Site_Mockup_Iphone5}
									notanimated
								/>
							</Cell>
							<Cell md={6} sm={12}>
								<StickyComponent>
									<Cell className='u-justifyCenter'>
										<Picture
											className='u-borderedPictures u-shadow6 u-marginY3 u-marginXAuto'
											src={OodibeeContent.Oodibee_Site_Mockup_Shop}
											notanimated
										/>
										<ProjectDescription
											title='Webdesign - Intégration'
											center
											right
										>
											Création des Mockups du Site Marchand Oodibee Page Galeries
											des Artistes
										</ProjectDescription>
									</Cell>
								</StickyComponent>
							</Cell>
						</Cell>
					</Container>
				</Container>
			</Loading>

			<Container>
				<Cell row>
					<FrameBorder animated>
						<Loading lazyLoadHeight={554} images={OodibeeContent.sectionSiteHeadings}>
							<Cell row>
								<Cell lg={9} md={12} disablePadding>
									<Cell md={4} sm={6}>
										<Picture
											className='u-shadow3'
											src={OodibeeContent.Oodibee_Site_Headings_Backplate1}
										/>
									</Cell>
									<Cell md={4} sm={6}>
										<Picture
											className='u-shadow3'
											src={OodibeeContent.Oodibee_Site_Headings_Backplate2}
										/>
									</Cell>
									<Cell md={4} sm={6}>
										<Picture
											className='u-shadow3'
											src={OodibeeContent.Oodibee_Site_Headings_Crea_Artistes}
										/>
									</Cell>
									<Cell md={4} sm={6}>
										<Picture
											className='u-shadow3'
											src={OodibeeContent.Oodibee_Site_Headings_Tableau}
										/>
									</Cell>
									<Cell md={4} sm={6}>
										<Picture
											className='u-shadow3'
											src={OodibeeContent.Oodibee_Site_Headings_Workspace_Artistes}
										/>
									</Cell>
								</Cell>
								<Cell lg={3} md={12}>
									<ProjectDescription title='Webdesign - Intégration' top>
										Déclinaison d’en-têtes spécifiques par page et catégorie
									</ProjectDescription>
								</Cell>
							</Cell>
						</Loading>

						<Loading lazyLoadHeight={796} images={OodibeeContent.sectionAppExplain}>
							<Cell row>
								<Cell>
									<Cell md={6} disablePadding>
										<AnimatedBackgoundPicture
											className='u-paddingY4 u-paddingX2'
											picture={OodibeeContent.Oodibee_App_Explain_Screen_Editeur}
										/>
									</Cell>
									<Cell md={6} disablePadding>
										<AnimatedBackgoundPicture
											className='u-paddingY4 u-paddingX2'
											picture={OodibeeContent.Oodibee_App_Explain_Screen_Gravure}
										/>
									</Cell>
								</Cell>
								<Cell lg={8} className='u-alignItemsStretch'>
									<Cell md={5} sm={7} disablePadding className='u-alignItemsStretch u-alignSelfStreitch'>
										<AnimatedBackgoundPicture
											className='u-paddingY4 u-paddingX3'
											picture={OodibeeContent.Oodibee_App_Explain_Retouches}
										/>
									</Cell>
									<Cell md={4} sm={5} disablePadding className='u-alignItemsStretch u-alignSelfStreitch'>
										<AnimatedBackgoundPicture
											className='u-paddingY4 u-paddingX3'
											picture={OodibeeContent.Oodibee_App_Explain_Ajustements}
										/>
									</Cell>
									<Cell md={3} sm={12} disablePadding className='u-alignItemsStretch u-alignSelfStreitch'>
										<AnimatedBackgoundPicture
											className='u-paddingY4 u-paddingX3 u-alignSelfStretch'
											picture={OodibeeContent.Oodibee_App_Explain_Calques}
										/>
									</Cell>
								</Cell>
								<Cell lg={4} md={8}>
									<AnimatedBackgoundPicture
										className='u-padding3'
										picture={OodibeeContent.Oodibee_App_Explain_Comptabilite}
									/>
									<ProjectDescription
										title='Visuels d’illustration'
										center
										className='u-marginTop2'
									>
										Création de visuels de présentation du personnalisateur
									</ProjectDescription>
								</Cell>
							</Cell>
						</Loading>

						<Loading lazyLoadHeight={870} images={OodibeeContent.sectionAppPreview}>
							<Cell row>
								<Cell>
									<ProjectDescription title='Design d’interface et intégration'>
										Création et intégration de l’interface d’un outil de
										personnalisation avec une visualisation en 3D des produits
										proposés
									</ProjectDescription>
								</Cell>
								<Cell md={5} sm={6}>
									<Picture
										className='u-shadow3'
										src={OodibeeContent.Oodibee_App_Preview_Step1}
									/>
								</Cell>
								<Cell md={5} sm={6}>
									<Picture
										className='u-shadow3'
										src={OodibeeContent.Oodibee_App_Preview_Step2}
									/>
								</Cell>
								<Cell md={5} sm={6} className='u-alignItemsFlexStart'>
									<Picture
										className='u-shadow3'
										src={OodibeeContent.Oodibee_App_Preview_Step3}
									/>
								</Cell>
								<Cell md={7} sm={12} className='u-alignItemsStretch u-marginTopAuto'>
									<Cell className='u-alignItemsFlexEnd' disablePadding dirColumn>
										<Cell disablePadding>
											<ProjectDescription title='Icônes' bottom right>
												Réalisation d’icones représentant les modèles
												disponibles dans le personnalisateur
											</ProjectDescription>
										</Cell>
										<Cell lg={2} disablePadding />
										<Cell lg={10} md={12} disablePadding>
											<Cell sm={2} xs={3}>
												<Picture src={OodibeeContent.Oodibee_Picto_Backplate} />
											</Cell>
											<Cell sm={2} xs={3}>
												<Picture src={OodibeeContent.Oodibee_Picto_Bumper} />
											</Cell>
											<Cell sm={2} xs={3}>
												<Picture src={OodibeeContent.Oodibee_Picto_Tableau} />
											</Cell>
											<Cell sm={2} xs={3}>
												<Picture src={OodibeeContent.Oodibee_Picto_TableauS} />
											</Cell>
											<Cell sm={2} xs={3}>
												<Picture src={OodibeeContent.Oodibee_Picto_TableauM} />
											</Cell>
											<Cell sm={2} xs={3}>
												<Picture src={OodibeeContent.Oodibee_Picto_TableauL} />
											</Cell>
											<Cell sm={2} xs={3}>
												<Picture src={OodibeeContent.Oodibee_Picto_Backplate_Iphone4} />
											</Cell>
											<Cell sm={2} xs={3}>
												<Picture
													src={OodibeeContent.Oodibee_Picto_Backplate_Iphone6Plus}
												/>
											</Cell>
											<Cell sm={2} xs={3}>
												<Picture src={OodibeeContent.Oodibee_Picto_Backplate_GalaxyS4} />
											</Cell>
											<Cell sm={2} xs={3}>
												<Picture
													src={OodibeeContent.Oodibee_Picto_Backplate_GalaxyNote4}
												/>
											</Cell>
											<Cell sm={2} xs={3}>
												<Picture src={OodibeeContent.Oodibee_Picto_Backplate_GalaxyS5} />
											</Cell>
											<Cell sm={2} xs={3}>
												<Picture src={OodibeeContent.Oodibee_Picto_Backplate_GalaxyS6} />
											</Cell>
										</Cell>
									</Cell>
								</Cell>
							</Cell>
						</Loading>
					</FrameBorder>
				</Cell>
			</Container>

			<Container>
				<Cell row>
					<FrameBorder animated>
						<Loading lazyLoadHeight={680} images={OodibeeContent.sectionSocial}>
							<Cell row>
								<Cell
									lg={6}
									md={12}
									disablePadding
									className='u-alignItemsCenter u-justifyCenter'
								>
									<Cell lg={6} md={5} sm={6}>
										<Picture src={OodibeeContent.Oodibee_Social_Post_GiftCard} />
									</Cell>
									<Cell lg={6} md={5} sm={6}>
										<Picture src={OodibeeContent.Oodibee_Social_Post_GiftCard_XMas} />
									</Cell>
									<Cell lg={6} md={5} sm={6}>
										<Picture src={OodibeeContent.Oodibee_Social_Post_St_Valentin} />
									</Cell>
									<Cell lg={6} md={5} sm={6}>
										<Picture src={OodibeeContent.Oodibee_Social_Post_Happy_XMas} />
									</Cell>
								</Cell>
								<Cell lg={6} md={12} className='u-alignItemsCenter u-justifyCenter'>
									<Picture src={OodibeeContent.Oodibee_Social_Post_Promo_XMas} />
								</Cell>
								<Cell>
									<ProjectDescription title='Communication Web' right>
										Réalisation de visuels accompagnant les posts majeurs sur
										les réseaux sociaux
									</ProjectDescription>
								</Cell>
							</Cell>
						</Loading>

						<Loading lazyLoadHeight={512} images={OodibeeContent.sectionSocialCouv}>
							<Cell row>
								<Cell>
									<ProjectDescription title='Communication Web'>
										Création de visuels de profil et de couverture pour les
										réseaux sociaux
									</ProjectDescription>
								</Cell>
								<Cell lg={10}>
									<Cell md={6} disablePadding>
										<AnimatedBackgoundPicture
											className='u-padding4'
											color='oodibee'
											picture={OodibeeContent.Oodibee_Social_Couv_Facebook}
										/>
									</Cell>
									<Cell md={6} disablePadding>
										<AnimatedBackgoundPicture
											className='u-padding4'
											color='oodibee'
											picture={OodibeeContent.Oodibee_Social_Couv_Twitter}
										/>
									</Cell>
								</Cell>
							</Cell>
						</Loading>
					</FrameBorder>
				</Cell>
			</Container>
		</ProjectPresentation>
	);
}
