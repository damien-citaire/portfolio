import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { Box, Picture } from '../index';

import './style.scss';

const PicturesRevealStrips = (props) => {
    let picturesRevealStripsClasses = classNames(props.className, 'picturesRevealStrips');

    return (
        <Box className={picturesRevealStripsClasses}>
            <Picture className="picturesRevealStrips-picture" src={props.pictureMain} notanimated />
            <Box className="picturesRevealStrips-strips">
                <Box className="picturesRevealStrips-strip picturesRevealStrips-stripTop" />
                <Box className="picturesRevealStrips-strip picturesRevealStrips-stripMiddle" />
                <Box className="picturesRevealStrips-strip picturesRevealStrips-stripBottom" />
            </Box>
        </Box>
    );
};

PicturesRevealStrips.propTypes = {
    className: PropTypes.string,
    picture: PropTypes.string,
    alt: PropTypes.string
};

export default PicturesRevealStrips;
