import React from 'react';
import PropTypes from 'prop-types';

// Defining the HTML tag that the component will support
const variantsMapping = {
	div: 'div',
	span: 'span'
};

const Box = ({ variant, children, ...props }) => {
	// If the variant exists in variantsMapping, we use it.
	// Otherwise, use div tag instead.
	const Component =
		variant ? variantsMapping[variant] :
		'div';

	return <Component {...props}>{children}</Component>;
};

Box.defaultProps = {
	variant: 'div'
};

Box.propTypes = {
	variant: PropTypes.oneOf(['div', 'span']),
	children: PropTypes.node
};

export default Box;
