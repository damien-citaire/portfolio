import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import {
	AnimatedBackgoundPicture,
	Box,
	Cell,
	Container,
	FrameBorder,
	ProjectDescription,
	ProjectsNavigation,
	Typography
} from '../index';

import './style.scss';

const ProjectBigHeader = (props) => {
	let ProjectBigHeaderClasses = classNames(props.className, 'projectBigHeader');
	let ProjectBigHeaderPictureClasses = classNames('projectBigHeader-picture', {
		'projectBigHeader-picture_noShadow': props.noShadow
	});

	return (
		<Container fluid className={ProjectBigHeaderClasses}>
			<Container justify='center' alignItems='stretch'>
				<FrameBorder className='projectBigHeader-frameBorder' animated />

				<Box className='projectBigHeader-bar'>
					<Box className='projectBigHeader-scrollMouse'>
						<Box className='projectBigHeader-scrollMouseIcon projectBigHeader-scrollMouseIcon_Body' />
						<Box className='projectBigHeader-scrollMouseIcon projectBigHeader-scrollMouseIcon_Arrow' />
					</Box>
				</Box>

				<Cell lg={6} className='projectBigHeader-content'>
					<Cell disablePaddingX>
						<Box className='projectBigHeader-text'>
							<ProjectsNavigation
								prevProject={props.prevProject}
								nextProject={props.nextProject}
								projectNumber={props.projectNumber}
							/>
							<Typography variant='h4' className='projectBigHeader-title'>
								{props.projectTitle}
							</Typography>
							{props.projectDescription}
						</Box>
					</Cell>
					<Cell disablePaddingX className='u-alignSelfFlexEnd'>
						<ProjectDescription
							title={props.projectSampleTitle}
							className='u-marginTop2 u-marginBottom0'
							right
						>
							{props.projectSampleDescription}
						</ProjectDescription>
					</Cell>
				</Cell>
				<Cell lg={6} className='u-alignSelfStretch'>
					<AnimatedBackgoundPicture
						className={ProjectBigHeaderPictureClasses}
						picture={props.projectSamplePicture}
					/>
				</Cell>
			</Container>
		</Container>
	);
};

ProjectBigHeader.defaultProps = {
	noShadow: false
};

ProjectBigHeader.propTypes = {
	className: PropTypes.string,
	noShadow: PropTypes.bool,
	projectTitle: PropTypes.string.isRequired,
	projectNumber: PropTypes.number.isRequired,
	projectDescription: PropTypes.object.isRequired,
	projectSamplePicture: PropTypes.string.isRequired,
	projectSampleTitle: PropTypes.string.isRequired,
	projectSampleDescription: PropTypes.string.isRequired,
	prevProject: PropTypes.string,
	nextProject: PropTypes.string
};

export default ProjectBigHeader;
