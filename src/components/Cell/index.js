import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { Box } from '../index';

import { componentPropsValidator } from 'utils';

import './style.scss';

const CELL_NUMBERS = [
	100,
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
	11,
	12
];
const CELL_DEFAULT_NUMBER = 12;

const Cell = ({
	className,
	children,
	dirColumn,
	disablePaddingLeft,
	disablePaddingRight,
	disablePaddingTop,
	disablePaddingBottom,
	disablePaddingX,
	disablePaddingY,
	disablePadding,
	row,
	xs,
	sm,
	md,
	lg,
	xl,
	...props
}) => {
	function cellNumberValidator(cellSize){
		let number = componentPropsValidator(cellSize, CELL_NUMBERS, CELL_DEFAULT_NUMBER);
		return number;
	}

	let cellClasses = classNames(
		className,
		'cell',
		{ 'u-paddingLeft0': disablePaddingLeft },
		{ 'u-paddingRight0': disablePaddingRight },
		{ 'u-paddingTop0': disablePaddingTop },
		{ 'u-paddingBottom0': disablePaddingBottom },
		{ 'u-paddingX0': disablePaddingX },
		{ 'u-paddingY0': disablePaddingY },
		{ 'u-padding0': disablePadding },
		{ cell_directionColumn: dirColumn },
		{ cell_row: row },
		{
			[`cell_xl${cellNumberValidator(xl)}`]: xl,
			[`cell_lg${cellNumberValidator(lg)}`]: lg,
			[`cell_md${cellNumberValidator(md)}`]: md,
			[`cell_sm${cellNumberValidator(sm)}`]: sm,
			[`cell_xs${cellNumberValidator(xs)}`]: xs
		}
	);

	return (
		<Box {...props} className={cellClasses}>
			{children}
		</Box>
	);
};

Cell.defaultProps = {
	xs: 12
};

Cell.propTypes = {
	className: PropTypes.string,
	children: PropTypes.node,
	dirColumn: PropTypes.bool,
	disablePaddingLeft: PropTypes.bool,
	disablePaddingRight: PropTypes.bool,
	disablePaddingTop: PropTypes.bool,
	disablePaddingBottom: PropTypes.bool,
	disablePaddingX: PropTypes.bool,
	disablePaddingY: PropTypes.bool,
	disablePadding: PropTypes.bool,
	row: PropTypes.bool,
	xs: PropTypes.oneOf(CELL_NUMBERS),
	sm: PropTypes.oneOf(CELL_NUMBERS),
	md: PropTypes.oneOf(CELL_NUMBERS),
	lg: PropTypes.oneOf(CELL_NUMBERS),
	xl: PropTypes.oneOf(CELL_NUMBERS)
};

export default Cell;
