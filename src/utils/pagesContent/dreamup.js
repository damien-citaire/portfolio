//
// Import Images
// ---
import DreamUp_Charte from 'assets/images/projects/DreamUp_Charte.jpg';
import DreamUp_App404 from 'assets/images/projects/DreamUp_App404.jpg';
import DreamUp_Animated_Logo from 'assets/images/projects/DreamUp_Animated_Logo.gif';
import DreamUp_AppSpinner3d from 'assets/images/projects/DreamUp_AppSpinner3d.gif';
import DreamUp_AppScreens_3DModel_Export from 'assets/images/projects/DreamUp_AppScreens_3DModel_Export.jpg';
import DreamUp_AppScreens_Branch from 'assets/images/projects/DreamUp_AppScreens_Branch.jpg';
import DreamUp_AppScreens_Catalog from 'assets/images/projects/DreamUp_AppScreens_Catalog.jpg';
import DreamUp_AppScreens_CatalogOpt_Charnieres from 'assets/images/projects/DreamUp_AppScreens_CatalogOpt_Charnieres.jpg';
import DreamUp_AppScreens_CatalogOpti_Face from 'assets/images/projects/DreamUp_AppScreens_CatalogOpti_Face.jpg';
import DreamUp_AppScreens_Config from 'assets/images/projects/DreamUp_AppScreens_Config.jpg';
import DreamUp_AppScreens_Config_Trace from 'assets/images/projects/DreamUp_AppScreens_Config_Trace.jpg';
import DreamUp_AppScreens_ListingModels from 'assets/images/projects/DreamUp_AppScreens_ListingModels.jpg';
import DreamUp_AppScreens_RecapCommand from 'assets/images/projects/DreamUp_AppScreens_RecapCommand.jpg';
import DreamUp_AppScreens_SearchClient from 'assets/images/projects/DreamUp_AppScreens_SearchClient.jpg';
import DreamUp_Blueprint_Components_pt1 from 'assets/images/projects/DreamUp_Blueprint_Components_pt1.png';
import DreamUp_Blueprint_Components_pt2 from 'assets/images/projects/DreamUp_Blueprint_Components_pt2.png';
import DreamUp_BonsComande_Formes from 'assets/images/projects/DreamUp_BonsComande_Formes.jpg';
import DreamUp_BonsComande_Outillage from 'assets/images/projects/DreamUp_BonsComande_Outillage.jpg';
import DreamUp_BonsComande_SurMesures from 'assets/images/projects/DreamUp_BonsComande_SurMesures.jpg';
import DreamUp_PrezVisu_2D_3D from 'assets/images/projects/DreamUp_PrezVisu_2D_3D.jpg';
import DreamUp_PrezVisu_Arrivee_Branches from 'assets/images/projects/DreamUp_PrezVisu_Arrivee_Branches.jpg';
import DreamUp_PrezVisu_Blueprint_Blue from 'assets/images/projects/DreamUp_PrezVisu_Blueprint_Blue.png';
import DreamUp_PrezVisu_Blueprint_White from 'assets/images/projects/DreamUp_PrezVisu_Blueprint_White.png';
import DreamUp_PrezVisu_Concept from 'assets/images/projects/DreamUp_PrezVisu_Concept.jpg';
import DreamUp_PrezVisu_Kit from 'assets/images/projects/DreamUp_PrezVisu_Kit.jpg';
import DreamUp_Print_Brochure_A4 from 'assets/images/projects/DreamUp_Print_Brochure_A4.jpg';
import DreamUp_Print_Cdv from 'assets/images/projects/DreamUp_Print_Cdv.png';
import DreamUp_Print_Invit_Envelope from 'assets/images/projects/DreamUp_Print_Invit_Envelope.jpg';
import DreamUp_Print_Invit_LunetteParty_Recto from 'assets/images/projects/DreamUp_Print_Invit_LunetteParty_Recto.jpg';
import DreamUp_Print_Invit_LunetteParty_Verso from 'assets/images/projects/DreamUp_Print_Invit_LunetteParty_Verso.jpg';
import DreamUp_Print_Invit_Silmo_Recto from 'assets/images/projects/DreamUp_Print_Invit_Silmo_Recto.jpg';
import DreamUp_Print_Invit_Silmo_Verso from 'assets/images/projects/DreamUp_Print_Invit_Silmo_Verso.jpg';
import DreamUp_Print_Affiche_Salon from 'assets/images/projects/DreamUp_Print_Affiche_Salon.png';
import DreamUp_Print_RollUp from 'assets/images/projects/DreamUp_Print_RollUp.png';
import DreamUp_Sitemap_FullInSituation from 'assets/images/projects/DreamUp_Sitemap_FullInSituation.png';
import DreamUp_Sitemap_PreviewPart from 'assets/images/projects/DreamUp_Sitemap_PreviewPart.png';
import DreamUp_Sitemap_Preview_Screen from 'assets/images/projects/DreamUp_Sitemap_Preview_Screen.png';
import DreamUp_Sitemap_Screens from 'assets/images/projects/DreamUp_Sitemap_Screens.png';
import DreamUp_Social_Facebook from 'assets/images/projects/DreamUp_Social_Facebook.jpg';
import DreamUp_Social_Twitter from 'assets/images/projects/DreamUp_Social_Twitter.jpg';
import DreamUp_Video_Components_3D_Glasses from 'assets/images/projects/DreamUp_Video_Components_3D_Glasses.svg';
import DreamUp_Video_Components_App_Photo from 'assets/images/projects/DreamUp_Video_Components_App_Photo.png';
import DreamUp_Video_Components_Morty_Buste from 'assets/images/projects/DreamUp_Video_Components_Morty_Buste.svg';
import DreamUp_Video_Components_Morty_Buste_Glasses from 'assets/images/projects/DreamUp_Video_Components_Morty_Buste_Glasses.svg';
import DreamUp_Video_Components_Morty_Faces from 'assets/images/projects/DreamUp_Video_Components_Morty_Faces.svg';
import DreamUp_Video_Components_Rick_Morty from 'assets/images/projects/DreamUp_Video_Components_Rick_Morty.png';
import DreamUp_Video_Components_Screen_4views from 'assets/images/projects/DreamUp_Video_Components_Screen_4views.png';
import DreamUp_Video_Components_Screen_Adaptation from 'assets/images/projects/DreamUp_Video_Components_Screen_Adaptation.png';
import DreamUp_Video_Components_Screen_Catalog from 'assets/images/projects/DreamUp_Video_Components_Screen_Catalog.png';
import DreamUp_Video_Components_Tool_Cephalo from 'assets/images/projects/DreamUp_Video_Components_Tool_Cephalo.png';
import DreamUp_Video_Components_Tool_Pupilo from 'assets/images/projects/DreamUp_Video_Components_Tool_Pupilo.png';
import DreamUp_Video_Components_Tool_Reglet from 'assets/images/projects/DreamUp_Video_Components_Tool_Reglet.png';
import DreamUp_Video_Components_Tool_Rhinometre from 'assets/images/projects/DreamUp_Video_Components_Tool_Rhinometre.png';
import DreamUp_Video_Components_Tools from 'assets/images/projects/DreamUp_Video_Components_Tools.png';
import DreamUp_Video_Components_iPad from 'assets/images/projects/DreamUp_Video_Components_iPad.png';
import DreamUp_WebSite_Designer from 'assets/images/projects/DreamUp_WebSite_Designer.jpg';
import DreamUp_WebSite_Formulaire from 'assets/images/projects/DreamUp_WebSite_Formulaire.jpg';
import DreamUp_WebSite_Index from 'assets/images/projects/DreamUp_WebSite_Index.jpg';
import DreamUp_WebSite_Opticien from 'assets/images/projects/DreamUp_WebSite_Opticien.jpg';
//

//
// Stock Images in const for Loading Sections
// ---
export const sectionPrez = [
	DreamUp_Charte,
];

export const sectionVisuelsPrez = [
	DreamUp_PrezVisu_Concept,
    DreamUp_PrezVisu_Blueprint_White,
    DreamUp_PrezVisu_Blueprint_Blue,
    DreamUp_PrezVisu_2D_3D,
    DreamUp_PrezVisu_Arrivee_Branches,
    DreamUp_PrezVisu_Kit
];

export const sectionSiteMap = [
	DreamUp_Sitemap_FullInSituation,
	DreamUp_Sitemap_PreviewPart,
	DreamUp_Sitemap_Screens,
	DreamUp_Sitemap_Preview_Screen
];

export const sectionBluePrintComponents = [
	DreamUp_Blueprint_Components_pt1,
	DreamUp_Blueprint_Components_pt2
];

export const sectionAnim404 = [
	DreamUp_Animated_Logo,
	DreamUp_App404,
	DreamUp_AppSpinner3d
];

export const sectionAppScreen = [
	DreamUp_AppScreens_ListingModels,
    DreamUp_AppScreens_Config_Trace,
    DreamUp_AppScreens_CatalogOpti_Face,
    DreamUp_AppScreens_Branch,
    DreamUp_AppScreens_3DModel_Export,
    DreamUp_AppScreens_Config,
    DreamUp_AppScreens_Catalog,
    DreamUp_AppScreens_CatalogOpt_Charnieres,
    DreamUp_AppScreens_RecapCommand,
    DreamUp_AppScreens_SearchClient
];

export const sectionBonCommande = [
	DreamUp_BonsComande_Formes,
	DreamUp_BonsComande_SurMesures,
	DreamUp_BonsComande_Outillage
];

export const sectionInvits = [
	DreamUp_Print_Invit_LunetteParty_Recto,
	DreamUp_Print_Invit_LunetteParty_Verso,
	DreamUp_Print_Invit_Silmo_Recto,
    DreamUp_Print_Invit_Silmo_Verso
];

export const sectionPrint = [
	DreamUp_Print_Brochure_A4,
	DreamUp_Print_Cdv
];

export const sectionPlv = [
	DreamUp_Print_Affiche_Salon,
	DreamUp_Print_RollUp
];

export const sectionVideoComponents = [
	DreamUp_Video_Components_Tool_Cephalo,
    DreamUp_Video_Components_Tool_Rhinometre,
    DreamUp_Video_Components_Tool_Reglet,
    DreamUp_Video_Components_3D_Glasses,
    DreamUp_Video_Components_iPad,
    DreamUp_Video_Components_App_Photo,
    DreamUp_Video_Components_Tool_Pupilo,
    DreamUp_Video_Components_Morty_Buste,
    DreamUp_Video_Components_Morty_Buste_Glasses,
    DreamUp_Video_Components_Morty_Faces,
    DreamUp_Video_Components_Rick_Morty,
    DreamUp_Video_Components_Screen_4views,
    DreamUp_Video_Components_Screen_Adaptation,
    DreamUp_Video_Components_Screen_Catalog
];

export const sectionWebsite = [
	DreamUp_WebSite_Designer,
    DreamUp_WebSite_Formulaire,
    DreamUp_WebSite_Index,
    DreamUp_WebSite_Opticien
];

export const sectionSocial = [
	DreamUp_Social_Facebook,
    DreamUp_Social_Twitter
];
//

//
// Export Images
// ---
export {
    DreamUp_Charte,
    DreamUp_App404,
    DreamUp_Animated_Logo,
    DreamUp_AppSpinner3d,
    DreamUp_AppScreens_Branch,
    DreamUp_AppScreens_3DModel_Export,
    DreamUp_AppScreens_Catalog,
    DreamUp_AppScreens_CatalogOpt_Charnieres,
    DreamUp_AppScreens_CatalogOpti_Face,
    DreamUp_AppScreens_Config,
    DreamUp_AppScreens_Config_Trace,
    DreamUp_AppScreens_ListingModels,
    DreamUp_AppScreens_RecapCommand,
    DreamUp_AppScreens_SearchClient,
    DreamUp_Blueprint_Components_pt1,
    DreamUp_Blueprint_Components_pt2,
    DreamUp_BonsComande_Formes,
    DreamUp_BonsComande_Outillage,
    DreamUp_BonsComande_SurMesures,
    DreamUp_PrezVisu_2D_3D,
    DreamUp_PrezVisu_Arrivee_Branches,
    DreamUp_PrezVisu_Blueprint_Blue,
    DreamUp_PrezVisu_Blueprint_White,
    DreamUp_PrezVisu_Concept,
    DreamUp_PrezVisu_Kit,
    DreamUp_Print_Brochure_A4,
    DreamUp_Print_Cdv,
    DreamUp_Print_Invit_Envelope,
    DreamUp_Print_Invit_LunetteParty_Recto,
    DreamUp_Print_Invit_LunetteParty_Verso,
    DreamUp_Print_Invit_Silmo_Recto,
    DreamUp_Print_Invit_Silmo_Verso,
    DreamUp_Print_Affiche_Salon,
    DreamUp_Print_RollUp,
    DreamUp_Sitemap_FullInSituation,
    DreamUp_Sitemap_PreviewPart,
    DreamUp_Sitemap_Preview_Screen,
    DreamUp_Sitemap_Screens,
    DreamUp_Social_Facebook,
    DreamUp_Social_Twitter,
    DreamUp_Video_Components_3D_Glasses,
    DreamUp_Video_Components_App_Photo,
    DreamUp_Video_Components_Morty_Buste,
    DreamUp_Video_Components_Morty_Buste_Glasses,
    DreamUp_Video_Components_Morty_Faces,
    DreamUp_Video_Components_Rick_Morty,
    DreamUp_Video_Components_Screen_4views,
    DreamUp_Video_Components_Screen_Adaptation,
    DreamUp_Video_Components_Screen_Catalog,
    DreamUp_Video_Components_Tool_Cephalo,
    DreamUp_Video_Components_Tool_Pupilo,
    DreamUp_Video_Components_Tool_Reglet,
    DreamUp_Video_Components_Tool_Rhinometre,
    DreamUp_Video_Components_Tools,
    DreamUp_Video_Components_iPad,
    DreamUp_WebSite_Designer,
    DreamUp_WebSite_Formulaire,
    DreamUp_WebSite_Index,
    DreamUp_WebSite_Opticien
}
//