//
// Import Images
// ---
import Home_Border from 'assets/images/home/Home_Border.svg';
import Home_Portfolio from 'assets/images/home/Home_Portfolio.svg';
import Home_Name from 'assets/images/home/Home_Name.svg';
import Home_Name_Outlines from 'assets/images/home/Home_Name_Outlines.svg';
import Home_Ondes from 'assets/images/home/Home_Ondes.svg';
import Home_Draw from 'assets/images/home/Home_Draw.svg';
import Home_Dev from 'assets/images/home/Home_Dev.svg';
import Home_Responsive from 'assets/images/home/Home_Responsive.svg';

//

//
// Stock Images in const for Loading Sections
// ---
export const images = [
    Home_Border,
    Home_Portfolio,
    Home_Name,
    Home_Name_Outlines,
    Home_Ondes,
    Home_Draw,
    Home_Dev,
    Home_Responsive
];
//

//
// Export Images
// ---
export {
    Home_Border,
    Home_Portfolio,
    Home_Name,
    Home_Name_Outlines,
    Home_Ondes,
    Home_Draw,
    Home_Dev,
    Home_Responsive
}
//