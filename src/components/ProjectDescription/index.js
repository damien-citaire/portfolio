import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { Box, Typography } from '../index';

import './style.scss';

const ProjectDescription = (props) => {
	let ProjectDescriptionClasses = classNames(
		props.className,
		'projectDescription',
		{ projectDescription_top: props.top },
		{ projectDescription_right: props.right },
		{ projectDescription_bottom: props.bottom },
		{ projectDescription_left: props.left },
		{ projectDescription_center: props.center }
	);

	return (
		<Box className={ProjectDescriptionClasses}>
			<Box variant='span' className='projectDescription-bar' />
			<Typography variant='subtitle1' className='projectDescription-title'>
				{props.title}
			</Typography>
			<Typography variant='body2' className='projectDescription-text'>
				{props.children}
			</Typography>
		</Box>
	);
};

ProjectDescription.propTypes = {
	className: PropTypes.string,
	title: PropTypes.string.isRequired,
	children: PropTypes.node.isRequired,
	top: PropTypes.bool,
	right: PropTypes.bool,
	bottom: PropTypes.bool,
	left: PropTypes.bool,
	center: PropTypes.bool
};

export default ProjectDescription;
