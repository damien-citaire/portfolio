import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { Box, Typography, Picture } from '../index';

import './style.scss';

const AboutInformation = (props) => {
	let AboutInformationClasses = classNames(props.className, 'aboutInformation');

	let Content =
		props.link ? <a
			className='aboutInformation-text aboutInformation-text_link'
			href={props.link}
			target='_blank'
			rel='noopener noreferrer'
		>
			{props.text}
		</a> :
		<Typography variant='caption1' color='secondary' className='aboutInformation-text'>
			{props.text}
		</Typography>;

	return (
		<Box className={AboutInformationClasses}>
			<Picture src={props.icon} className='aboutInformation-icon' notanimated />
			{Content}
		</Box>
	);
};

AboutInformation.propTypes = {
	className: PropTypes.string,
	text: PropTypes.string.isRequired,
	icon: PropTypes.string.isRequired
};

export default AboutInformation;
