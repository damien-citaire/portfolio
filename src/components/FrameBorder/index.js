import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { Box } from '../index';

import './style.scss';

const FrameBorder = (props) => {
	let frameBorderClasses = classNames(props.className, 'frameBorder', {
		frameBorder_animated: props.animated
	});

	return (
		<Box {...props} className={frameBorderClasses}>
			<Box className="frameBorder-lineWrapper frameBorder_vertical frameBorder_left">
                <Box className="frameBorder-line"/>
            </Box>
			<Box className="frameBorder-lineWrapper frameBorder_horizontal frameBorder_top">
                <Box className="frameBorder-line"/>
            </Box>
			<Box className="frameBorder-content">{props.children}</Box>
			<Box className="frameBorder-lineWrapper frameBorder_vertical frameBorder_right">
                <Box className="frameBorder-line"/>
            </Box>
			<Box className="frameBorder-lineWrapper frameBorder_horizontal frameBorder_bottom">
                <Box className="frameBorder-line"/>
            </Box>
		</Box>
	);
};

FrameBorder.defaultProps = {
	animated: false
};

FrameBorder.propTypes = {
	className: PropTypes.string,
	animated: PropTypes.bool,
	children: PropTypes.node
};

export default FrameBorder;
