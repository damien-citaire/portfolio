import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { Box } from '../index';

import { validatedFormatedProps } from 'utils';

import "./style.scss";

const CONTAINER_DIRECTIONS = ['row', 'row-reverse', 'column', 'column-reverse'];
const CONTAINER_DEFAULT_DIRECTION = 'row';
const CONTAINER_JUSTIFYS = ['flex-start', 'center', 'flex-end', 'space-between', 'space-around', 'space-evenly'];
const CONTAINER_DEFAULT_JUSTIFY = 'flex-start';
const CONTAINER_ALIGN_ITEMS = ['flex-start', 'center', 'flex-end', 'stretch', 'baseline'];
const CONTAINER_DEFAULT_ALIGN_ITEM = 'center';

const Container = (props) => {
    

    let containerDirectionValidator = validatedFormatedProps(props.direction, CONTAINER_DIRECTIONS, CONTAINER_DEFAULT_DIRECTION);
    let containerJustifyValidator = validatedFormatedProps(props.justify, CONTAINER_JUSTIFYS, CONTAINER_DEFAULT_JUSTIFY);
    let containerAlignItemsValidator = validatedFormatedProps(props.alignItems, CONTAINER_ALIGN_ITEMS, CONTAINER_DEFAULT_ALIGN_ITEM);


    let containerClasses = classNames(
        props.className,
        'container', 
        `u-direction${containerDirectionValidator}`,
        `u-justify${containerJustifyValidator}`,
        `u-alignItems${containerAlignItemsValidator}`,
        { 'container_fluid': props.fluid },
    );


    return (
        <Box className={containerClasses}>
            {props.children}
        </Box>
    );
};

Container.defaultProps = {
    direction: CONTAINER_DEFAULT_DIRECTION,
    justify: CONTAINER_DEFAULT_JUSTIFY,
    alignItems: CONTAINER_DEFAULT_ALIGN_ITEM,
}

Container.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node.isRequired,
    fluid: PropTypes.bool,
    direction: PropTypes.oneOf(CONTAINER_DIRECTIONS),
    justify: PropTypes.oneOf(CONTAINER_JUSTIFYS),
    alignItems: PropTypes.oneOf(CONTAINER_ALIGN_ITEMS),
};

export default Container;