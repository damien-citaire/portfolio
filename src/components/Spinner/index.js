import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { Box, Typography } from 'components/index';

import './style.scss';

const Spinner = (props) => {
	let spinnerClasses = classNames('spinner', {
		spinner_fullScreen: props.fullScreen
	});
	return (
		<Box className={spinnerClasses}>
			<Typography variant='overline' color='secondary' className='spinner-label'>
				{props.message}
			</Typography>
		</Box>
	);
};

Spinner.defaultProps = {
	fullScreen: false,
	message: 'Chargement des images',
};

Spinner.propTypes = {
	message: PropTypes.string,
	fullScreen: PropTypes.bool
};

export default Spinner;
