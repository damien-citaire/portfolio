import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { Box, Picture } from '../index';

import './style.scss';

const AnimatedBackgoundPictureOffset = (props) => {
    let animatedBackgoundPictureOffsetClasses = classNames(
        props.className,
        'animatedBackgroundPictureOffset',
    );

    return (
        <Box className={animatedBackgoundPictureOffsetClasses}>
            <Box className="animatedBackgroundPictureOffset-pictureWrapper">
                <Picture
                    className="animatedBackgroundPictureOffset-picture"
                    src={props.picture}
                    alt={props.alt}
                />
            </Box>
            <Box variant='span' className="animatedBackgroundPictureOffset-background" />
        </Box>
    );
};

AnimatedBackgoundPictureOffset.defaultProps = {
    alt: '',
};

AnimatedBackgoundPictureOffset.propTypes = {
    className: PropTypes.string,
    picture: PropTypes.string,
    alt: PropTypes.string,
};

export default AnimatedBackgoundPictureOffset;
