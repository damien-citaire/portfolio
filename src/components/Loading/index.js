import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import LazyLoad from 'react-lazyload';
import { Spinner } from 'components/index';

const Loading = (props) => {
	const [
		imgsLoaded,
		setImgsLoaded
	] = useState(false);

	useEffect(() => {
		const $images = props.images;

		const loadImage = (image) => {
			return new Promise((resolve, reject) => {
				const loadImg = new Image();
				loadImg.src = image;
				loadImg.onload = () => resolve(image);
				loadImg.onerror = (err) => reject(err);
			});
		};

		Promise.all($images.map((image) => loadImage(image)))
			.then(() => setImgsLoaded(true))
			.catch((err) => console.log('Failed to load images', err));
	});

	return (
		<React.Fragment>
			{
				imgsLoaded ? <LazyLoad height={props.lazyLoadHeight} offset={props.lazyLoadOffset}>
					{ props.children }
				</LazyLoad> :
				<Spinner/>}
		</React.Fragment>
	);
};

Loading.defaultProps = {
	lazyLoadHeight: 200,
	lazyLoadOffset: -200
};

Loading.propTypes = {
	images: PropTypes.array.isRequired,
	lazyLoadHeight: PropTypes.number.isRequired,
	lazyLoadOffset: PropTypes.number,
	children: PropTypes.node.isRequired
};

export default Loading;
