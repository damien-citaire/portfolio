import React from 'react';

import { Typography } from 'components/index';

const PageProjects = () => {
	return (
		<div className='App'>
			<header className='App-header'>
				<Typography variant='h6'>
					This app is currently in version {process.env.REACT_APP_VERSION}
				</Typography>
        <br />
				<Typography
					variant='h6'
					style={{
						borderBottom: 'solid 1px',
						paddingBottom: '12px'
					}}
				>
					Type system implementation :
				</Typography>

				<Typography variant='h1' color='primary'>
					Heading1
				</Typography>
				<Typography variant='h2' color='secondary'>
					Heading2
				</Typography>
				<Typography variant='h3'>Heading3</Typography>
				<Typography variant='h4' color='primary'>
					Heading4
				</Typography>
				<Typography variant='h5' color='primary'>
					Heading5
				</Typography>
				<Typography variant='h6' color='primary'>
					Heading6
				</Typography>
				<Typography variant='subtitle1' color='primary'>
					Subtitle1
				</Typography>
				<Typography variant='subtitle2' color='secondary'>
					Subtitle2
				</Typography>
				<Typography variant='body1' color='secondary'>
					Body1
				</Typography>
				<Typography variant='body2' color='secondary'>
					Body2
				</Typography>
				<Typography variant='body3' color='secondary'>
					Body3
				</Typography>
				<Typography variant='caption1' color='secondary'>
					Caption1
				</Typography>
				<Typography variant='caption2' color='secondary'>
					Caption2
				</Typography>
				<Typography variant='caption3' color='secondary'>
					Caption3
				</Typography>
				<Typography variant='caption4' color='secondary'>
					Caption4
				</Typography>
				<Typography variant='overline' color='secondary'>
					Overline
				</Typography>
			</header>
		</div>
	);
};

export default PageProjects;
