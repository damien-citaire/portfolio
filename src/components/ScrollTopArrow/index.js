import React, { useState, useEffect } from 'react';
import './style.scss';
import classNames from 'classnames';

const ScrollTopArrow = () => {
	const [
		showScroll,
		setShowScroll
	] = useState(false);

	useEffect(() => {
		window.addEventListener('scroll', checkScrollTop);
		return function cleanup(){
			window.removeEventListener('scroll', checkScrollTop);
		};
	});

	const checkScrollTop = () => {
		if (!showScroll && window.pageYOffset > 1200) {
			setShowScroll(true);
		}
		else if (showScroll && window.pageYOffset <= 1200) {
			setShowScroll(false);
		}
	};

	const scrollTop = () => {
		window.scrollTo({ top: 0, behavior: 'smooth' });
	};

	let scrollTopArrowClasses = classNames('scrollTopArrow', {
		'isActive': showScroll
	});

	return (
		<div
			className={scrollTopArrowClasses}
			onClick={scrollTop}
		/>
	);
};

export default ScrollTopArrow;
