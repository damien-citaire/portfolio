import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { Box, Typography } from '../index';

import './style.scss';

const AboutSection = (props) => {
	let AboutSectionClasses = classNames(props.className, 'aboutSection');

	return (
		<Box className={AboutSectionClasses}>
			<Box className='aboutSection-dateWrapper'>
				<Typography
					variant='caption1'
					color='secondary'
					className='aboutSection-date aboutSection-dateBegin'
				>
					{props.dateBeginMonth} <Box variant='span' className='aboutSection-dateYear'>{props.dateBeginYear}</Box>
				</Typography>
				<Typography
					variant='caption1'
					color='secondary'
					className='aboutSection-date aboutSection-dateEnd'
				>
					{props.dateEndMonth} <Box variant='span' className='aboutSection-dateYear'>{props.dateEndYear}</Box>
				</Typography>
			</Box>
			<Box>
				<Typography
					variant='subtitle1'
					color='secondary'
					className='aboutSection-text aboutSection-title'
				>
					{props.title}
				</Typography>
				<Typography
					variant='subtitle2'
					color='primary'
					className='aboutSection-text aboutSection-subtitle'
				>
					{props.subtitle}
				</Typography>
				<Box className='aboutSection-content'>{props.children}</Box>
				<Box className='aboutSection-separator' />
			</Box>
		</Box>
	);
};

AboutSection.propTypes = {
	className: PropTypes.string,
	title: PropTypes.string.isRequired,
	subtitle: PropTypes.string.isRequired,
	dateBeginMonth: PropTypes.string.isRequired,
	dateBeginYear: PropTypes.string.isRequired,
	dateEndMonth: PropTypes.string.isRequired,
	dateEndYear: PropTypes.string.isRequired,
	children: PropTypes.node
};

export default AboutSection;
