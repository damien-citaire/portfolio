import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './style.scss';


const Video = (props) => {
    let videoClasses = classNames(
        props.className,
        'video',
    );

    return (
        <video
            src={props.src}
            type="video/mp4"
            controls={props.controls}
            className={videoClasses}
        />
    );
};

Video.defaultProps = {
    controls: false,
}

Video.propTypes = {
    className: PropTypes.string,
    src: PropTypes.string.isRequired,
    controls: PropTypes.bool,
};

export default Video;
