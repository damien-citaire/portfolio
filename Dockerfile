FROM node:14 as build

RUN git clone https://gitlab.com/damien-citaire/portfolio.git

WORKDIR /portfolio

RUN yarn install && \
    yarn run build && \
    mv build/* public/ && \
    cp public/index.html public/404.html

FROM nginx:1.19

COPY --from=build /portfolio/public /usr/share/nginx/html

## Commandes : 
# docker build -t portfolio . --no-cache
# docker run -it --rm -d -p 8080:80 --name portfolio portfolio
