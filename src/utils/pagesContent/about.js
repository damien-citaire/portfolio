//
// Import Images
// ---
import Prez_ProfilPict from 'assets/images/about/Prez_ProfilPict.jpg';
import Prez_CV_Car from 'assets/images/about/Prez_CV_Car.svg';
import Prez_CV_Home from 'assets/images/about/Prez_CV_Home.svg';
import Prez_CV_Mail from 'assets/images/about/Prez_CV_Mail.svg';
import Prez_CV_Phone from 'assets/images/about/Prez_CV_Phone.svg';
import Prez_CV_Linkedin from 'assets/images/about/Prez_CV_Linkedin.svg';
import Prez_CV_Gitlab from 'assets/images/about/Prez_CV_Gitlab.svg';
import Prez_SkillsLogo_AdobeAi from 'assets/images/about/Prez_SkillsLogo_AdobeAi.svg';
import Prez_SkillsLogo_AdobeAe from 'assets/images/about/Prez_SkillsLogo_AdobeAe.svg';
import Prez_SkillsLogo_AdobeId from 'assets/images/about/Prez_SkillsLogo_AdobeId.svg';
import Prez_SkillsLogo_AdobePs from 'assets/images/about/Prez_SkillsLogo_AdobePs.svg';
import Prez_SkillsLogo_Bootstrap from 'assets/images/about/Prez_SkillsLogo_Bootstrap.svg';
import Prez_SkillsLogo_CSS3 from 'assets/images/about/Prez_SkillsLogo_CSS3.svg';
import Prez_SkillsLogo_HTML5 from 'assets/images/about/Prez_SkillsLogo_HTML5.svg';
import Prez_SkillsLogo_Git from 'assets/images/about/Prez_SkillsLogo_Git.svg';
import Prez_SkillsLogo_Invision from 'assets/images/about/Prez_SkillsLogo_Invision.svg';
import Prez_SkillsLogo_JS from 'assets/images/about/Prez_SkillsLogo_JS.svg';
import Prez_SkillsLogo_MaterialDesign from 'assets/images/about/Prez_SkillsLogo_MaterialDesign.svg';
import Prez_SkillsLogo_Mui from 'assets/images/about/Prez_SkillsLogo_Mui.svg';
import Prez_SkillsLogo_PHP from 'assets/images/about/Prez_SkillsLogo_PHP.svg';
import Prez_SkillsLogo_React from 'assets/images/about/Prez_SkillsLogo_React.svg';
import Prez_SkillsLogo_Sass from 'assets/images/about/Prez_SkillsLogo_Sass.svg';
import Prez_SkillsLogo_Sketch from 'assets/images/about/Prez_SkillsLogo_Sketch.svg';
import Prez_SkillsLogo_SQL from 'assets/images/about/Prez_SkillsLogo_SQL.svg';
import Prez_SkillsLogo_Symfony from 'assets/images/about/Prez_SkillsLogo_Symfony.svg';
import Prez_SkillsLogo_Wordpress from 'assets/images/about/Prez_SkillsLogo_Wordpress.svg';
import Prez_Interest_Sport from 'assets/images/about/Prez_Interest_Sport.svg';
import Prez_Interest_Bricolage from 'assets/images/about/Prez_Interest_Bricolage.svg';
import Prez_Interest_Cinema from 'assets/images/about/Prez_Interest_Cinema.svg';
//

//
// Stock Images in const for Loading Sections
// ---
export const images = [
    Prez_ProfilPict,
    Prez_CV_Car,
    Prez_CV_Home,
    Prez_CV_Mail,
    Prez_CV_Phone,
    Prez_CV_Linkedin,
    Prez_CV_Gitlab,
    Prez_SkillsLogo_AdobeAi,
    Prez_SkillsLogo_AdobeAe,
    Prez_SkillsLogo_AdobeId,
    Prez_SkillsLogo_AdobePs,
    Prez_SkillsLogo_Bootstrap,
    Prez_SkillsLogo_CSS3,
    Prez_SkillsLogo_HTML5,
    Prez_SkillsLogo_Git,
    Prez_SkillsLogo_Invision,
    Prez_SkillsLogo_JS,
    Prez_SkillsLogo_MaterialDesign,
    Prez_SkillsLogo_Mui,
    Prez_SkillsLogo_PHP,
    Prez_SkillsLogo_React,
    Prez_SkillsLogo_Sass,
    Prez_SkillsLogo_Sketch,
    Prez_SkillsLogo_SQL,
    Prez_SkillsLogo_Symfony,
    Prez_SkillsLogo_Wordpress,
    Prez_Interest_Sport,
    Prez_Interest_Bricolage,
    Prez_Interest_Cinema
];
//

//
// Export Images
// ---
export {
    Prez_ProfilPict,
    Prez_CV_Car,
    Prez_CV_Home,
    Prez_CV_Mail,
    Prez_CV_Phone,
    Prez_CV_Linkedin,
    Prez_CV_Gitlab,
    Prez_SkillsLogo_AdobeAi,
    Prez_SkillsLogo_AdobeAe,
    Prez_SkillsLogo_AdobeId,
    Prez_SkillsLogo_AdobePs,
    Prez_SkillsLogo_Bootstrap,
    Prez_SkillsLogo_CSS3,
    Prez_SkillsLogo_HTML5,
    Prez_SkillsLogo_Git,
    Prez_SkillsLogo_Invision,
    Prez_SkillsLogo_JS,
    Prez_SkillsLogo_MaterialDesign,
    Prez_SkillsLogo_Mui,
    Prez_SkillsLogo_PHP,
    Prez_SkillsLogo_React,
    Prez_SkillsLogo_Sass,
    Prez_SkillsLogo_Sketch,
    Prez_SkillsLogo_SQL,
    Prez_SkillsLogo_Symfony,
    Prez_SkillsLogo_Wordpress,
    Prez_Interest_Sport,
    Prez_Interest_Bricolage,
    Prez_Interest_Cinema
}
//