//
// Import Images
// ---
import Oodibee_App_Explain_Ajustements from 'assets/images/projects/Oodibee_App_Explain_Ajustements.png';
import Oodibee_App_Explain_Calques from 'assets/images/projects/Oodibee_App_Explain_Calques.png';
import Oodibee_App_Explain_Comptabilite from 'assets/images/projects/Oodibee_App_Explain_Comptabilite.png';
import Oodibee_App_Explain_Retouches from 'assets/images/projects/Oodibee_App_Explain_Retouches.png';
import Oodibee_App_Explain_Screen_Editeur from 'assets/images/projects/Oodibee_App_Explain_Screen_Editeur.jpg';
import Oodibee_App_Explain_Screen_Gravure from 'assets/images/projects/Oodibee_App_Explain_Screen_Gravure.jpg';
import Oodibee_App_Preview_Step1 from 'assets/images/projects/Oodibee_App_Preview_Step1.jpg';
import Oodibee_App_Preview_Step2 from 'assets/images/projects/Oodibee_App_Preview_Step2.jpg';
import Oodibee_App_Preview_Step3 from 'assets/images/projects/Oodibee_App_Preview_Step3.jpg';
import Oodibee_Charte from 'assets/images/projects/Oodibee_Charte.jpg';
import Oodibee_Crea_Perso_Cat from 'assets/images/projects/Oodibee_Crea_Perso_Cat.png';
import Oodibee_Crea_Perso_Eagle from 'assets/images/projects/Oodibee_Crea_Perso_Eagle.png';
import Oodibee_Crea_Perso_Escape from 'assets/images/projects/Oodibee_Crea_Perso_Escape.png';
import Oodibee_Crea_Perso_Kiss from 'assets/images/projects/Oodibee_Crea_Perso_Kiss.png';
import Oodibee_Crea_Perso_Preview_Cat from 'assets/images/projects/Oodibee_Crea_Perso_Preview_Cat.jpg';
import Oodibee_Crea_Perso_Preview_Eagle from 'assets/images/projects/Oodibee_Crea_Perso_Preview_Eagle.jpg';
import Oodibee_Crea_Perso_Preview_Escape from 'assets/images/projects/Oodibee_Crea_Perso_Preview_Escape.jpg';
import Oodibee_Crea_Perso_Preview_Kiss from 'assets/images/projects/Oodibee_Crea_Perso_Preview_Kiss.jpg';
import Oodibee_Indications_Bumper from 'assets/images/projects/Oodibee_Indications_Bumper.png';
import Oodibee_Indications_Dibon from 'assets/images/projects/Oodibee_Indications_Dibon.png';
import Oodibee_Indications_Essences_Dibond from 'assets/images/projects/Oodibee_Indications_Essences_Dibond.png';
import Oodibee_Indications_Scotch from 'assets/images/projects/Oodibee_Indications_Scotch.png';
import Oodibee_Picto_Backplate_GalaxyNote4 from 'assets/images/projects/Oodibee_Picto_Backplate_GalaxyNote4.png';
import Oodibee_Picto_Backplate_GalaxyS4 from 'assets/images/projects/Oodibee_Picto_Backplate_GalaxyS4.png';
import Oodibee_Picto_Backplate_GalaxyS5 from 'assets/images/projects/Oodibee_Picto_Backplate_GalaxyS5.png';
import Oodibee_Picto_Backplate_GalaxyS6 from 'assets/images/projects/Oodibee_Picto_Backplate_GalaxyS6.png';
import Oodibee_Picto_Backplate_Iphone4 from 'assets/images/projects/Oodibee_Picto_Backplate_Iphone4.png';
import Oodibee_Picto_Backplate_Iphone5 from 'assets/images/projects/Oodibee_Picto_Backplate_Iphone5.png';
import Oodibee_Picto_Backplate_Iphone6 from 'assets/images/projects/Oodibee_Picto_Backplate_Iphone6.png';
import Oodibee_Picto_Backplate_Iphone6Plus from 'assets/images/projects/Oodibee_Picto_Backplate_Iphone6Plus.png';
import Oodibee_Picto_Backplate from 'assets/images/projects/Oodibee_Picto_Backplate.png';
import Oodibee_Picto_Bumper from 'assets/images/projects/Oodibee_Picto_Bumper.png';
import Oodibee_Picto_Tableau from 'assets/images/projects/Oodibee_Picto_Tableau.png';
import Oodibee_Picto_TableauL from 'assets/images/projects/Oodibee_Picto_TableauL.png';
import Oodibee_Picto_TableauM from 'assets/images/projects/Oodibee_Picto_TableauM.png';
import Oodibee_Picto_TableauS from 'assets/images/projects/Oodibee_Picto_TableauS.png';
import Oodibee_PrezProduit_Bumper from 'assets/images/projects/Oodibee_PrezProduit_Bumper.png';
import Oodibee_PrezProduit_Plaques from 'assets/images/projects/Oodibee_PrezProduit_Plaques.png';
import Oodibee_PrezProduit_Smartphones from 'assets/images/projects/Oodibee_PrezProduit_Smartphones.png';
import Oodibee_PrezProduit_Tableaux from 'assets/images/projects/Oodibee_PrezProduit_Tableaux.png';
import Oodibee_Site_Mockup_Iphone5 from 'assets/images/projects/Oodibee_Site_Mockup_Iphone5.jpg';
import Oodibee_Site_Mockup_Shop from 'assets/images/projects/Oodibee_Site_Mockup_Shop.jpg';
import Oodibee_Site_Headings_Backplate1 from 'assets/images/projects/Oodibee_Site_Headings_Backplate1.jpg';
import Oodibee_Site_Headings_Backplate2 from 'assets/images/projects/Oodibee_Site_Headings_Backplate2.jpg';
import Oodibee_Site_Headings_Crea_Artistes from 'assets/images/projects/Oodibee_Site_Headings_Crea_Artistes.jpg';
import Oodibee_Site_Headings_Tableau from 'assets/images/projects/Oodibee_Site_Headings_Tableau.jpg';
import Oodibee_Site_Headings_Workspace_Artistes from 'assets/images/projects/Oodibee_Site_Headings_Workspace_Artistes.jpg';
import Oodibee_Social_Couv_Facebook from 'assets/images/projects/Oodibee_Social_Couv_Facebook.jpg';
import Oodibee_Social_Couv_Twitter from 'assets/images/projects/Oodibee_Social_Couv_Twitter.jpg';
import Oodibee_Social_Post_St_Valentin from 'assets/images/projects/Oodibee_Social_Post_St_Valentin.jpg';
import Oodibee_Social_Post_GiftCard_XMas from 'assets/images/projects/Oodibee_Social_Post_GiftCard_XMas.jpg';
import Oodibee_Social_Post_GiftCard from 'assets/images/projects/Oodibee_Social_Post_GiftCard.jpg';
import Oodibee_Social_Post_Happy_XMas from 'assets/images/projects/Oodibee_Social_Post_Happy_XMas.jpg';
import Oodibee_Social_Post_Promo_XMas from 'assets/images/projects/Oodibee_Social_Post_Promo_XMas.jpg';
import Oodibee_Vignette_Essence_Acajou from 'assets/images/projects/Oodibee_Vignette_Essence_Acajou.png';
import Oodibee_Vignette_Essence_Ebene from 'assets/images/projects/Oodibee_Vignette_Essence_Ebene.png';
import Oodibee_Vignette_Essence_Evene from 'assets/images/projects/Oodibee_Vignette_Essence_Evene.png';
import Oodibee_Vignette_Essence_Movengi from 'assets/images/projects/Oodibee_Vignette_Essence_Movengi.png';
import Oodibee_Vignette_Essence_Poirier from 'assets/images/projects/Oodibee_Vignette_Essence_Poirier.png';
//

//
// Stock Images in const for Loading Sections
// ---
export const sectionPrez = [
	Oodibee_Charte,
];

export const sectionVisuelsPrez = [
	Oodibee_PrezProduit_Bumper,
    Oodibee_PrezProduit_Smartphones,
    Oodibee_PrezProduit_Tableaux,
    Oodibee_PrezProduit_Plaques
];

export const sectionVisuelsIndic = [
	Oodibee_Indications_Bumper,
	Oodibee_Indications_Dibon,
	Oodibee_Indications_Scotch,
	Oodibee_Indications_Essences_Dibond
];

export const sectionVisuelsAlveoles = [
	Oodibee_Vignette_Essence_Acajou,
	Oodibee_Vignette_Essence_Ebene,
	Oodibee_Vignette_Essence_Evene,
    Oodibee_Vignette_Essence_Movengi,
    Oodibee_Vignette_Essence_Poirier
];

export const sectionCreaPerso = [
	Oodibee_Crea_Perso_Cat,
	Oodibee_Crea_Perso_Eagle,
	Oodibee_Crea_Perso_Escape,
    Oodibee_Crea_Perso_Kiss,
    Oodibee_Crea_Perso_Preview_Eagle,
    Oodibee_Crea_Perso_Preview_Escape,
    Oodibee_Crea_Perso_Preview_Kiss,
    Oodibee_Crea_Perso_Preview_Cat
];

export const sectionWebsite = [
	Oodibee_Site_Mockup_Iphone5,
	Oodibee_Site_Mockup_Shop
];

export const sectionSiteHeadings = [
	Oodibee_Site_Headings_Backplate1,
	Oodibee_Site_Headings_Backplate2,
	Oodibee_Site_Headings_Crea_Artistes,
    Oodibee_Site_Headings_Tableau,
    Oodibee_Site_Headings_Workspace_Artistes
];

export const sectionAppExplain = [
	Oodibee_App_Explain_Screen_Editeur,
	Oodibee_App_Explain_Screen_Gravure,
	Oodibee_App_Explain_Retouches,
    Oodibee_App_Explain_Ajustements,
    Oodibee_App_Explain_Calques,
    Oodibee_App_Explain_Comptabilite
];

export const sectionAppPreview = [
	Oodibee_App_Preview_Step1,
	Oodibee_App_Preview_Step2,
	Oodibee_App_Preview_Step3,
    Oodibee_Picto_Backplate,
    Oodibee_Picto_Bumper,
    Oodibee_Picto_Tableau,
    Oodibee_Picto_TableauS,
	Oodibee_Picto_TableauM,
	Oodibee_Picto_TableauL,
    Oodibee_Picto_Backplate_Iphone4,
    Oodibee_Picto_Backplate_Iphone6Plus,
    Oodibee_Picto_Backplate_GalaxyS4,
    Oodibee_Picto_Backplate_GalaxyNote4,
    Oodibee_Picto_Backplate_GalaxyS5,
    Oodibee_Picto_Backplate_GalaxyS6
];

export const sectionSocial = [
	Oodibee_Social_Post_GiftCard,
	Oodibee_Social_Post_GiftCard_XMas,
	Oodibee_Social_Post_St_Valentin,
    Oodibee_Social_Post_Happy_XMas,
    Oodibee_Social_Post_Promo_XMas
];

export const sectionSocialCouv = [
	Oodibee_Social_Couv_Facebook,
	Oodibee_Social_Couv_Twitter
];
//

//
// Export Images
// ---
export {
    Oodibee_App_Explain_Ajustements,
    Oodibee_App_Explain_Calques,
    Oodibee_App_Explain_Comptabilite,
    Oodibee_App_Explain_Retouches,
    Oodibee_App_Explain_Screen_Editeur,
    Oodibee_App_Explain_Screen_Gravure,
    Oodibee_App_Preview_Step1,
    Oodibee_App_Preview_Step2,
    Oodibee_App_Preview_Step3,
    Oodibee_Charte,
    Oodibee_Crea_Perso_Cat,
    Oodibee_Crea_Perso_Eagle,
    Oodibee_Crea_Perso_Escape,
    Oodibee_Crea_Perso_Kiss,
    Oodibee_Crea_Perso_Preview_Cat,
    Oodibee_Crea_Perso_Preview_Eagle,
    Oodibee_Crea_Perso_Preview_Escape,
    Oodibee_Crea_Perso_Preview_Kiss,
    Oodibee_Indications_Bumper,
    Oodibee_Indications_Dibon,
    Oodibee_Indications_Essences_Dibond,
    Oodibee_Indications_Scotch,
    Oodibee_Picto_Backplate_GalaxyNote4,
    Oodibee_Picto_Backplate_GalaxyS4,
    Oodibee_Picto_Backplate_GalaxyS5,
    Oodibee_Picto_Backplate_GalaxyS6,
    Oodibee_Picto_Backplate_Iphone4,
    Oodibee_Picto_Backplate_Iphone5,
    Oodibee_Picto_Backplate_Iphone6,
    Oodibee_Picto_Backplate_Iphone6Plus,
    Oodibee_Picto_Backplate,
    Oodibee_Picto_Bumper,
    Oodibee_Picto_Tableau,
    Oodibee_Picto_TableauL,
    Oodibee_Picto_TableauM,
    Oodibee_Picto_TableauS,
    Oodibee_PrezProduit_Bumper,
    Oodibee_PrezProduit_Plaques,
    Oodibee_PrezProduit_Smartphones,
    Oodibee_PrezProduit_Tableaux,
    Oodibee_Site_Mockup_Iphone5,
    Oodibee_Site_Mockup_Shop,
    Oodibee_Site_Headings_Backplate1,
    Oodibee_Site_Headings_Backplate2,
    Oodibee_Site_Headings_Crea_Artistes,
    Oodibee_Site_Headings_Tableau,
    Oodibee_Site_Headings_Workspace_Artistes,
    Oodibee_Social_Couv_Facebook,
    Oodibee_Social_Couv_Twitter,
    Oodibee_Social_Post_St_Valentin,
    Oodibee_Social_Post_GiftCard_XMas,
    Oodibee_Social_Post_GiftCard,
    Oodibee_Social_Post_Happy_XMas,
    Oodibee_Social_Post_Promo_XMas,
    Oodibee_Vignette_Essence_Acajou,
    Oodibee_Vignette_Essence_Ebene,
    Oodibee_Vignette_Essence_Evene,
    Oodibee_Vignette_Essence_Movengi,
    Oodibee_Vignette_Essence_Poirier
}
//