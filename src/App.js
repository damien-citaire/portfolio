import React, { Suspense } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import 'assets/style/index.scss';

import { Box, AppMenu, ScrollTop } from 'components';

import { routes } from 'utils/routes';

export default function App(){
	return (
		<Suspense
			fallback={
				<Box
					style={{
						height: '100vh',
						width: '100%',
						display: 'flex',
						alignItems: 'center',
						justifyContent: 'center'
					}}
				>
					Le chargement va bientôt se lancer...
				</Box>
			}
		>
			<BrowserRouter basename={process.env.PUBLIC_URL}>
				<Box className='app'>
					<ScrollTop>
						{routes.map(({ path, Component, name }) => (
							<Route exact key={path} path={path}>
								{name !== 'Home' && <AppMenu />}
								<Component />
							</Route>
						))}
					</ScrollTop>
				</Box>
			</BrowserRouter>
		</Suspense>
	);
}
