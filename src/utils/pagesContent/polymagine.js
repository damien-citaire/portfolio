//
// Import Images
// ---
import Polymagine_Charte from 'assets/images/projects/Polymagine_Charte.jpg';
import Polymagine_CdV from 'assets/images/projects/Polymagine_CdV.png';
import Polymagine_Prez_Lissac01 from 'assets/images/projects/Polymagine_Prez_Lissac01.jpg';
import Polymagine_Prez_Lissac02 from 'assets/images/projects/Polymagine_Prez_Lissac02.jpg';
import Polymagine_Prez_Lissac03 from 'assets/images/projects/Polymagine_Prez_Lissac03.jpg';
import Polymagine_Prez_Lissac04 from 'assets/images/projects/Polymagine_Prez_Lissac04.jpg';
import Polymagine_Prez_Lissac05 from 'assets/images/projects/Polymagine_Prez_Lissac05.jpg';
import Polymagine_Prez_Lissac06 from 'assets/images/projects/Polymagine_Prez_Lissac06.jpg';
import Polymagine_Prez_Lissac07_1 from 'assets/images/projects/Polymagine_Prez_Lissac07_1.jpg';
import Polymagine_Prez_Lissac07_2 from 'assets/images/projects/Polymagine_Prez_Lissac07_2.jpg';
import Polymagine_Schema_Illu1 from 'assets/images/projects/Polymagine_Schema_Illu1.png';
import Polymagine_Schema_Illu2 from 'assets/images/projects/Polymagine_Schema_Illu2.png';
import Polymagine_Schema_Illu3 from 'assets/images/projects/Polymagine_Schema_Illu3.png';
import Polymagine_Schema_PlateForme1 from 'assets/images/projects/Polymagine_Schema_PlateForme1.png';
import Polymagine_Schema_PlateForme2 from 'assets/images/projects/Polymagine_Schema_PlateForme2.png';
import Polymagine_Schema_PlateForme3 from 'assets/images/projects/Polymagine_Schema_PlateForme3.png';
import Polymagine_Schema_Dreamup from 'assets/images/projects/Polymagine_Schema_Dreamup.png';
import Polymagine_Website_Icons from 'assets/images/projects/Polymagine_Website_Icons.svg';
import Polymagine_Website_Screen_Technologie from 'assets/images/projects/Polymagine_Website_Screen_Technologie.jpg';
import Polymagine_Website_Screen_Contact from 'assets/images/projects/Polymagine_Website_Screen_Contact.jpg';
import Polymagine_Website_Screen_Dreamup from 'assets/images/projects/Polymagine_Website_Screen_Dreamup.jpg';
import Polymagine_Website_Screen_Index from 'assets/images/projects/Polymagine_Website_Screen_Index.jpg';
//

//
// Stock Images in const for Loading Sections
// ---
export const sectionPrez = [
	Polymagine_Charte,
];

export const sectionPrint = [
	Polymagine_CdV
];

export const sectionSchemas = [
	Polymagine_Schema_PlateForme2,
	Polymagine_Schema_PlateForme3,
	Polymagine_Schema_PlateForme1
];

export const sectionSchemasAdvanced = [
	Polymagine_Schema_Dreamup,
	Polymagine_Schema_Illu1,
    Polymagine_Schema_Illu2,
    Polymagine_Schema_Illu3
];

export const sectionPrezLissac = [
	Polymagine_Prez_Lissac01,
    Polymagine_Prez_Lissac02,
    Polymagine_Prez_Lissac03,
	Polymagine_Prez_Lissac04,
    Polymagine_Prez_Lissac05,
    Polymagine_Prez_Lissac06,
    Polymagine_Prez_Lissac07_1,
    Polymagine_Prez_Lissac07_2
];

export const sectionWebsiteIcons = [
	Polymagine_Website_Icons
];

export const sectionWebsite = [
    Polymagine_Website_Screen_Index,
    Polymagine_Website_Screen_Contact,
    Polymagine_Website_Screen_Technologie,
    Polymagine_Website_Screen_Dreamup
];
//

//
// Export Images
// ---
export {
    Polymagine_Charte,
    Polymagine_CdV,
    Polymagine_Prez_Lissac01,
    Polymagine_Prez_Lissac02,
    Polymagine_Prez_Lissac03,
    Polymagine_Prez_Lissac04,
    Polymagine_Prez_Lissac05,
    Polymagine_Prez_Lissac06,
    Polymagine_Prez_Lissac07_1,
    Polymagine_Prez_Lissac07_2,
    Polymagine_Schema_Illu1,
    Polymagine_Schema_Illu2,
    Polymagine_Schema_Illu3,
    Polymagine_Schema_PlateForme1,
    Polymagine_Schema_PlateForme2,
    Polymagine_Schema_PlateForme3,
    Polymagine_Schema_Dreamup,
    Polymagine_Website_Icons,
    Polymagine_Website_Screen_Technologie,
    Polymagine_Website_Screen_Contact,
    Polymagine_Website_Screen_Dreamup,
    Polymagine_Website_Screen_Index
}
//