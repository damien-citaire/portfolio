# Damien CITAIRE - Portfolio

Presentation of my **career**, my **skills** and various **professional projects** carried out **since 2014** :bowtie:

## The website

The website is build with [Create React App](https://create-react-app.dev/).

### Style gestion

The **homemade naming convention for style classes** used here is based on the methodology of [SUIT CSS](https://suitcss.github.io/).

It follow these rules :

```scss
.componentName {}
.componentName_modifierName {}
.componentName-descendantName {}
.componentName-descendantName_modifierName {}
.componentName.isStateOfComponent {}

// Utility classes are prefixed with u-
```

The order of the style properties is based on [SMACSS](http://smacss.com/).

The rest of the [Sass](https://sass-lang.com/) style follow the rules defined in the `stylelint.config.js` file.



