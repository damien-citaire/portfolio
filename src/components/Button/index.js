import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { NavLink } from 'react-router-dom';

import './style.scss';

const Button = (props) => {
	let buttonClasses = classNames(props.className, 'button');

	return (
		<NavLink exact key={props.path} to={props.path}>
			<button className={buttonClasses} type='button'>
				{props.label}
			</button>
		</NavLink>
	);
};

Button.defaultProps = {
	label: 'I am a button',
	color: 'primary'
};

Button.propTypes = {
	className: PropTypes.string,
	label: PropTypes.string,
};

export default Button;
